const passport = require('passport')
require('./services/passport.service')(passport)

to = function(promise) {//global function that will help use handle promise rejections, this article talks about it http://blog.grossman.io/how-to-write-async-await-without-try-catch-blocks-in-javascript/
    return promise
    .then(data => {
        return [null, data]
    }).catch(err =>
        [pe(err)]
    )
}

pe = require('parse-error')//parses error so you can read error message and handle them accordingly

throwError = function(err_message, log){ // TE stands for Throw Error
    throw new Error(err_message)
}

errorMessage = function(res, err, code){ // Error Web Response
    if(typeof err == 'object' && typeof err.message != 'undefined'){
        err = err.message
    }

    if(typeof code !== 'undefined') res.statusCode = code

    return res.json({success:false, error: err})
}

successMessage = function(res, data, code){ // Success Web Response
    let send_data = {success:true}

    if(typeof data == 'object'){
        send_data = Object.assign(data, send_data)//merge the objects
    }

    if(typeof code !== 'undefined') res.statusCode = code

    return res.json(send_data)
}

// requireAuth = passport.authenticate('jwt', {session: false})

//This is here to handle all the uncaught promise rejections
process.on('unhandledRejection', error => {
    console.error('Uncaught Error', pe(error))
})




