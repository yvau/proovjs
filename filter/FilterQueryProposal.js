/* ============
 * Property Transformer
 * ============
 *
 * The transformer for the property.
 */

let FilterQuery = require('./FilterQuery')

module.exports = class FilterQueryProposal extends FilterQuery {
  /**
   * Method used to transform a fetched property.
   *
   * @param proposal The fetched property.
   *
   * @returns {Object} The transformed property.
   */ 
  static fetch (proposal) {
    const object = {}
      if (proposal.status) {
        object.status = proposal.status
      }
      if (!proposal.status) {
        object.status = 'is_active'
      }
      if (proposal.typeOfProposal) {
        object.type_of_proposal = proposal.typeOfProposal
      }
      return object
  }

  /**
   * Method used to transform a send role.
   *
   * @param role The role to be send.
   *
   * @returns {Object} The transformed role.
   */
  static send (property) {
    let formData = new FormData()
    let images = store.state.global.list
    for (let x in images) {
      formData.append('propertyPhotos[]', images[x].file)
    }
    formData.append('status', property.status)
    formData.append('characteristics', property.characteristics.toString())
    formData.append('description', property.description)
    formData.append('address', property.address)
    formData.append('postalCode', property.postalCode)
    formData.append('bathrooms', property.bathrooms)
    formData.append('bedrooms', property.bedrooms)
    formData.append('price', property.price)
    formData.append('saleType', property.saleType)
    formData.append('size', property.size)
    formData.append('type', property.type)
    formData.append('location', (property.location.id === undefined) ? '' : property.location.id.toString())
    return formData
  }
}
