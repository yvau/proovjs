/* ============
 * Property Transformer
 * ============
 *
 * The transformer for the property.
 */

let FilterQuery = require('./FilterQuery')

module.exports = class FilterQueryPackage extends FilterQuery {
  /**
   * Method used to transform a fetched property.
   *
   * @param proposal The fetched property.
   *
   * @returns {Object} The transformed property.
   */ 
  static fetch (pack) {
    const object = {}
      if (pack.t) {
        object.type = pack.t
      }
      return object
  }
}
