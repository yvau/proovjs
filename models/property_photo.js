/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('property_photo', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    content_type: {
      type: DataTypes.STRING,
      allowNull: true
    },
    date_of_creation: {
      type: DataTypes.DATE,
      allowNull: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    size: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    thumbnail_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    thumbnail_size: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    type: {
      type: DataTypes.STRING,
      allowNull: true
    },
    url: {
      type: DataTypes.STRING,
      allowNull: true
    },
    property_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'property',
        key: 'id'
      }
    }
  }, {
    tableName: 'property_photo'
  });
};
