/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('payment', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    count_for_rent: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    count_for_sale: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    customer_id_paypal: {
      type: DataTypes.STRING,
      allowNull: true
    },
    customer_id_stripe: {
      type: DataTypes.STRING,
      allowNull: true
    },
    type: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'payment'
  });
};
