/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('auto_increment', {
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    increment_number: {
      type: DataTypes.BIGINT,
      allowNull: true
    }
  }, {
    tableName: 'auto_increment',
    timestamps: false
  });
};
