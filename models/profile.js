/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('profile', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    account_non_expired: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    account_non_locked: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    credential: {
      type: DataTypes.STRING,
      allowNull: true
    },
    credentials_non_expired: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    date_of_creation: {
      type: DataTypes.DATE,
      allowNull: true
    },
    date_of_creation_token: {
      type: DataTypes.DATE,
      allowNull: true
    },
    enabled: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    ip_address: {
      type: DataTypes.STRING,
      allowNull: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: true
    },
    role: {
      type: DataTypes.STRING,
      allowNull: true
    },
    token: {
      type: DataTypes.STRING,
      allowNull: true
    },
    payment_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'payment',
        key: 'id'
      }
    },
    profile_information_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'profile_information',
        key: 'id'
      }
    }
  }, {
    tableName: 'profile'
  });
};
