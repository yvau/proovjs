/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('payment_details', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    date_of_creation: {
      type: DataTypes.DATE,
      allowNull: true
    },
    id_transaction: {
      type: DataTypes.STRING,
      allowNull: true
    },
    method: {
      type: DataTypes.STRING,
      allowNull: true
    },
    package_product_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'package_product',
        key: 'id'
      }
    },
    payment_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'payment',
        key: 'id'
      }
    }
  }, {
    tableName: 'payment_details'
  });
};
