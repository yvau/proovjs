/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('profile_information', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    accreditation_number: {
      type: DataTypes.STRING,
      allowNull: true
    },
    agent_type: {
      type: DataTypes.STRING,
      allowNull: true
    },
    best_way_to_reach_you: {
      type: DataTypes.STRING,
      allowNull: true
    },
    birth_date: {
      type: DataTypes.STRING,
      allowNull: true
    },
    email_contact: {
      type: DataTypes.STRING,
      allowNull: true
    },
    first_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    gender: {
      type: DataTypes.STRING,
      allowNull: true
    },
    home_phone: {
      type: DataTypes.STRING,
      allowNull: true
    },
    last_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    name_of_banner: {
      type: DataTypes.STRING,
      allowNull: true
    },
    office_phone: {
      type: DataTypes.STRING,
      allowNull: true
    },
    profile_photo_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'profile_photo',
        key: 'id'
      }
    }
  }, {
    tableName: 'profile_information',
    timestamps: false
  });
};
