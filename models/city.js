/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('city', {
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    alternate_names: {
      type: DataTypes.STRING,
      allowNull: true
    },
    f_code: {
      type: DataTypes.STRING,
      allowNull: true
    },
    latitude: {
      type: DataTypes.STRING,
      allowNull: true
    },
    longitude: {
      type: DataTypes.STRING,
      allowNull: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    name_ascii: {
      type: DataTypes.STRING,
      allowNull: true
    },
    timezone: {
      type: DataTypes.STRING,
      allowNull: true
    },
    province_id: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'province',
        key: 'id'
      }
    }
  }, {
    tableName: 'city'
  });
};
