/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('feed_activity', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    date_of_creation: {
      type: DataTypes.DATE,
      allowNull: true
    },
    is_interesting: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    is_read: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    verb: {
      type: DataTypes.STRING,
      allowNull: true
    },
    profile_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'profile',
        key: 'id'
      }
    },
    proposal_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'proposal',
        key: 'id'
      }
    },
    proposal_has_property_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'proposal_has_property',
        key: 'id'
      }
    }
  }, {
    tableName: 'feed_activity'
  });
};
