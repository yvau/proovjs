/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('proposal_statistic', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    count_proposition: {
      type: DataTypes.STRING,
      allowNull: true
    },
    count_view: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'proposal_statistic'
  });
};
