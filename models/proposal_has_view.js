/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('proposal_has_view', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    date_of_creation: {
      type: DataTypes.DATE,
      allowNull: true
    },
    profile_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'profile',
        key: 'id'
      }
    },
    proposal_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'proposal',
        key: 'id'
      }
    }
  }, {
    tableName: 'proposal_has_view'
  });
};
