/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('proposal_has_location', {
    proposal_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'proposal',
        key: 'id'
      }
    },
    location_id: {
      type: DataTypes.STRING,
      allowNull: false,
      references: {
        model: 'location',
        key: 'id'
      }
    }
  }, {
    tableName: 'proposal_has_location'
  });
};
