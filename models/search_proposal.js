/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('search_proposal', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    age_of_property: {
      type: DataTypes.STRING,
      allowNull: true
    },
    amenity: {
      type: DataTypes.STRING,
      allowNull: true
    },
    category_of_property: {
      type: DataTypes.STRING,
      allowNull: true
    },
    has_garage: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    has_lift: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    has_parking: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    has_pool: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    is_first_buyer: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    is_furnished: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    is_near_navigable_body_of_water: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    is_near_parc: {
      type: DataTypes.STRING,
      allowNull: true
    },
    is_near_public_transport: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    is_near_trade_square: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    is_pre_approved: {
      type: DataTypes.STRING,
      allowNull: true
    },
    is_pre_qualified: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    is_suitable_for_reduced_mobility: {
      type: DataTypes.STRING,
      allowNull: true
    },
    is_urgent: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    is_without_contingency: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    price_maximum: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    price_minimum: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    type_of_property: {
      type: DataTypes.STRING,
      allowNull: true
    },
    type_of_proposal: {
      type: DataTypes.STRING,
      allowNull: true
    },
    type_of_search: {
      type: DataTypes.STRING,
      allowNull: true
    },
    profile_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'profile',
        key: 'id'
      }
    }
  }, {
    tableName: 'search_proposal'
  });
};
