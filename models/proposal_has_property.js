/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('proposal_has_property', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    date_of_creation: {
      type: DataTypes.DATE,
      allowNull: true
    },
    is_interesting: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    payment_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'payment',
        key: 'id'
      }
    },
    property_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'property',
        key: 'id'
      }
    },
    proposal_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'proposal',
        key: 'id'
      }
    }
  }, {
    tableName: 'proposal_has_property'
  });
};
