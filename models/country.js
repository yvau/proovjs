/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('country', {
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    continent: {
      type: DataTypes.STRING,
      allowNull: true
    },
    currency_code: {
      type: DataTypes.STRING,
      allowNull: true
    },
    currency_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    languages: {
      type: DataTypes.STRING,
      allowNull: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true
    },
    postal_code: {
      type: DataTypes.STRING,
      allowNull: true
    },
    postal_format: {
      type: DataTypes.STRING,
      allowNull: true
    },
    tld: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'country'
  });
};
