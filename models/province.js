/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('province', {
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    name_ascii: {
      type: DataTypes.STRING,
      allowNull: true
    },
    country_id: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'country',
        key: 'id'
      }
    }
  }, {
    tableName: 'province'
  });
};
