/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('has_bookmark', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    date_of_creation: {
      type: DataTypes.DATE,
      allowNull: true
    },
    profile_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'profile',
        key: 'id'
      }
    },
    property_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'property',
        key: 'id'
      }
    },
    proposal_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'proposal',
        key: 'id'
      }
    }
  }, {
    tableName: 'has_bookmark'
  });
};
