/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('location', {
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    address: {
      type: DataTypes.STRING,
      allowNull: true
    },
    postal_code: {
      type: DataTypes.STRING,
      allowNull: true
    },
    city_id: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'city',
        key: 'id'
      }
    },
    country_id: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'country',
        key: 'id'
      }
    },
    province_id: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'province',
        key: 'id'
      }
    }
  }, {
    tableName: 'location'
  });
};
