/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('search_has_location', {
    search_proposal_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'search_proposal',
        key: 'id'
      }
    },
    location_id: {
      type: DataTypes.STRING,
      allowNull: false,
      references: {
        model: 'location',
        key: 'id'
      }
    }
  }, {
    tableName: 'search_has_location'
  });
};
