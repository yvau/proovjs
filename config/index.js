let config = {
  "development": {
    "jwt_secret": "secret",
    "database": {
      "username": "postgres",
      "password": "postgres",
      "database": "proov",
      "host": "127.0.0.1",
			"dialect": "postgres",
			"define": {
				"timestamps": false
			}
		},
		"mail": {
		  "smtp": "localhost",
		  "port": "2500",
			"secure": false,
			"tls":"true",
		  "auth": {
				"user": "test@test.Com",
				"pass": ""
			}
	  },
	  "cloudinary": {
	    "name": "dc5rinehl",
	    "key": "623142116591724",
	    "secret": "MV9Bm2F2pcJcA5MM0Ho47MHL1mU"
	  }
  },
  "production": {
    "jwt_secret": "MiOiJsb3JhLWFwcC1zZXJ2ZXIiLCJhdWQiOiJsb3JhLWFwcC",
	  "database": {
	    "username": "postgres",
	    "password": "postgres",
	    "database": "proov",
	    "host": "127.0.0.1",
		  "dialect": "postgres",
			"define": {
			  "timestamps": false
			}
	  },
	  "mail": {
		  "smtp": "smtp.gmail.com",
		  "port": "465",
			"secure": true,
			"tls":"false",
		  "auth": {
				"user": "mobiot.fabrice@gmail.com",
				"pass": "@@@Abcdefgh000"
			}
	  },
	  "cloudinary": {
	    "name": "dc5rinehl",
	    "key": "623142116591724",
	    "secret": "MV9Bm2F2pcJcA5MM0Ho47MHL1mU"
	  }
  },
  "test": {
    "jwt_secret": "test",
	  "database": {
	    "username": "postgres",
	    "password": "postgres",
	    "database": "proov",
	    "host": "127.0.0.1",
      "dialect": "postgres"
	  },
	  "cloudinary": {
      "name": "dc5rinehl",
      "key": "623142116591724",
      "secret": "MV9Bm2F2pcJcA5MM0Ho47MHL1mU"
	  }
  }
}
 
module.exports = config;