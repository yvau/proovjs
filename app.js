var createError = require('http-errors')
var express = require('express')
var favicon = require('express-favicon')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
var passport = require('passport')
require('./global_functions')

var app = express()

// setup favicon
app.use(favicon(__dirname + '/public/favicon.ico'))

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(require('./load.routes.js'))
app.use(passport.initialize())
app.use('/static', express.static(path.join(__dirname, 'public/static')))

if (process.env.NODE_ENV === 'development') {
  app.disable('etag')
}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404))
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
