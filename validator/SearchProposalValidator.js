const { check, validationResult } = require('express-validator/check');
let model = require('../models/index')

// class to validate form 
module.exports = class SearchProposalValidator {

  /**
   * Method used validate register form.
   */
  sellForm () {
    // create strategy for the form registration
    let errors = [
      check('ageOfProperty').not().isEmpty().withMessage('munst not be null'),
      check('categoryOfProperty').not().isEmpty().withMessage('munst not be null'),
      check('hasGarage').not().isEmpty().withMessage('munst not be null'),
      check('hasLift').not().isEmpty().withMessage('munst not be null'),
      check('hasParking').not().isEmpty().withMessage('munst not be null'),
      check('hasPool').not().isEmpty().withMessage('munst not be null'),
      check('isFirstBuyer').not().isEmpty().withMessage('munst not be null'),
      check('isNearNavigableBodyOfWater').not().isEmpty().withMessage('munst not be null'),
      check('isNearParc').not().isEmpty().withMessage('munst not be null'),
      check('isNearPublicTransport').not().isEmpty().withMessage('munst not be null'),
      check('isNearTradeSquare').not().isEmpty().withMessage('munst not be null'),
      check('isPreApproved').not().isEmpty().withMessage('munst not be null'),
      check('isPreQualified').not().isEmpty().withMessage('munst not be null'),
      check('isSuitableForReducedMobility').not().isEmpty().withMessage('munst not be null'),
      check('isUrgent').not().isEmpty().withMessage('munst not be null'),
      check('isWithoutContingency').not().isEmpty().withMessage('munst not be null'),
      check('priceMaximum').not().isEmpty().withMessage('munst not be null'),
      check('priceMinimum').not().isEmpty().withMessage('munst not be null'),
      check('typeOfProperty').not().isEmpty().withMessage('munst not be null'),
      check('typeOfProposal').not().isEmpty().withMessage('munst not be null'),
      check('typeOfSearch').not().isEmpty().withMessage('munst not be null'),
      check('amenity').not().isEmpty().withMessage('munst not be null')
    ]

    return errors
  }

    /**
   * Method used validate register form.
   */
  leaseForm () {
    // create strategy for the form registration
    let errors = [
      check('ageOfProperty').not().isEmpty().withMessage('munst not be null'),
      check('categoryOfProperty').not().isEmpty().withMessage('munst not be null'),
      check('hasGarage').not().isEmpty().withMessage('munst not be null'),
      check('hasLift').not().isEmpty().withMessage('munst not be null'),
      check('hasParking').not().isEmpty().withMessage('munst not be null'),
      check('hasPool').not().isEmpty().withMessage('munst not be null'),
      check('isFirstBuyer').not().isEmpty().withMessage('munst not be null'),
      check('isNearNavigableBodyOfWater').not().isEmpty().withMessage('munst not be null'),
      check('isNearParc').not().isEmpty().withMessage('munst not be null'),
      check('isNearPublicTransport').not().isEmpty().withMessage('munst not be null'),
      check('isNearTradeSquare').not().isEmpty().withMessage('munst not be null'),
      check('isPreApproved').not().isEmpty().withMessage('munst not be null'),
      check('isPreQualified').not().isEmpty().withMessage('munst not be null'),
      check('isSuitableForReducedMobility').not().isEmpty().withMessage('munst not be null'),
      check('isUrgent').not().isEmpty().withMessage('munst not be null'),
      check('isWithoutContingency').not().isEmpty().withMessage('munst not be null'),
      check('priceMaximum').not().isEmpty().withMessage('munst not be null'),
      check('priceMinimum').not().isEmpty().withMessage('munst not be null'),
      check('typeOfProperty').not().isEmpty().withMessage('munst not be null'),
      check('typeOfProposal').not().isEmpty().withMessage('munst not be null'),
      check('typeOfSearch').not().isEmpty().withMessage('munst not be null'),
      check('amenity').not().isEmpty().withMessage('munst not be null')
    ]

    return errors
  }
}
