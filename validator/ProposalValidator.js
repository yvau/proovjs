const { check, validationResult } = require('express-validator/check');
let model = require('../models/index')

// class to validate form 
module.exports = class ProposalValidator {

  /**
   * Method used validate register form.
   */
  buyNew () {
    // create strategy for the form registration
    let errors = [

    // fields location
    check('location').not().isEmpty().withMessage('must not be null'),

      // fields ageOfProperty
    check('ageOfProperty').not().isEmpty().withMessage('must not be null'),

    // fields credential
    check('bathrooms').not().isEmpty().withMessage('munst not be null'),

    //
    check('bedrooms').not().isEmpty().withMessage('munst not be null'),
    check('bedrooms').isInt().withMessage('please enter a number'),

    //
    check('isFurnished').not().isEmpty().withMessage('munst not be null'),

    //
    check('priceMaximum').not().isEmpty().withMessage('munst not be null'),
    check('priceMaximum').custom((value, { req }) => req.body.priceMinimum < value  ).withMessage('the max price must be greater'),
    // check('priceMaximum').not().isDecimal().withMessage('dec'),

    //
    check('priceMinimum').not().isEmpty().withMessage('munst not be null'),

    //
    check('size').not().isEmpty().withMessage('munst not be null'),

    //
    check('status').not().isEmpty().withMessage('munst not be null'),
    
    //
    check('typeOfProperty').not().isEmpty().withMessage('munst not be null')
    ]

    return errors
  }
}
