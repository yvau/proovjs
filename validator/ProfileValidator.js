const { check, validationResult } = require('express-validator/check');
let model = require('../models/index')

// class to validate form 
module.exports = class ProfileValidator {

  /**
   * Method used validate register form.
   */
  setEditInformation () {
    // create strategy for the form registration
    let errors = [
    // fieds lastname
    check('lastName').not().isEmpty().withMessage('must not be null'),

    // fields credential
    check('credential').not().isEmpty().withMessage('munst not be null'),
    check('credential').isEmail().withMessage('is not email'),
    check('credential').custom(value => {
      return model.profile.findOne({
        where: {credential: value} }).then(user => {
        if (user) {
          return Promise.reject('E-mail already in use')
        }
      })
    }),

    // fields gender
    check('gender').not().isEmpty().withMessage('must not be null'),

    //fields birthdate
    check('birthDate').not().isEmpty().withMessage('must not be null'),
    check('birthDate').custom((value, { req }) => moment(req.body.birthDate, 'YYY-MM-DD').isValid()).withMessage('pattern dont match'),

    // fields password
    check('password').not().isEmpty().withMessage('must not be null'),
    check('password').custom((value, { req }) => /((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})/.test(value)).withMessage('pattern dont match'),

    // fields repassword
    check('rePassword').not().isEmpty().withMessage('must not be null'),
    check('rePassword').custom((value, { req }) => value === req.body.password).withMessage('pattern dont match'),
    ]

    return errors
  }

  setEditPassword () {
    // create strategy for the form registration
    let errors = [
    // fieds oldPassword
    check('oldPassword').not().isEmpty().withMessage('must not be null'),

    // fields passsword
    check('password').not().isEmpty().withMessage('munst not be null'),
    // Min 1 special character. 2] Min 1 number. 3] Min 8 characters or More
    check('password').custom((value, { req }) => /^(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,}$/.test(value)).withMessage('pattern dont match'),
    // bcrypt



    // fields repassword
    check('rePassword').not().isEmpty().withMessage('must not be null'),
    check('rePassword').custom((value, { req }) => value === req.body.password).withMessage('pattern dont match'),
    ]

    return errors
  }

  setEditRole () {
    // create strategy for the form registration
    let errors = [
    // fieds roles
    check('role').not().isEmpty().withMessage('must not be null'),
    check('role').custom((value, { req }) => value.split(',').every( e => ['ROLE_BUYER', 'ROLE_SELLER', 'ROLE_TENANT', 'ROLE_LESSOR'].includes(e) )).withMessage('the role(s) entered arent taken into account'),
    ]

    return errors
  }

  setEditContact () {
    // create strategy for the form registration
    let errors = [
    // fieds bestWayToReachYou
    check('bestWayToReachYou').not().isEmpty().withMessage('must not be null'),

    // fields office number
    check('officePhone').custom((value, { req }) => (req.body.bestWayToReachYou === 'office_phone' && value === null) ? false : true ).withMessage('the role(s) entered arent taken into account'),
    // fields email
    check('emailContact').custom((value, { req }) => (req.body.bestWayToReachYou === 'email_contact' && value === null) ? false : true ).withMessage('the role(s) entered arent taken into account'),
    // fields 
    check('homePhone').custom((value, { req }) => (req.body.bestWayToReachYou === 'home_phone' && value === null) ? false : true ).withMessage('the role(s) entered arent taken into account'),
    ]

    return errors
  }
}
