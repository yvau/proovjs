const { check, validationResult } = require('express-validator/check');
let model = require('../models/index')

// class to validate form 
module.exports = class PropertyValidator {

  /**
   * Method used validate register form.
   */
  property () {
    // create strategy for the form registration
    let errors = [
      // fields bathrooms
      check('bathrooms').not().isEmpty().withMessage('must not be null'),
      check('bathrooms').isInt().withMessage('must not be a number'),
      
      // fields bedrooms
      check('bedrooms').not().isEmpty().withMessage('must not be null'),
      check('bedrooms').isInt().withMessage('must not a number'),
      
      // fields price
      check('price').not().isEmpty().withMessage('must not be null'),
      check('price').isDecimal().withMessage('must not be null'),

      // fields sale_type
      check('saleType').not().isEmpty().withMessage('must not be null'),
      check('saleType').isIn(['for_sale', 'for_rent']).withMessage('the sale not taken into account'),

      // fields size
      check('size').not().isEmpty().withMessage('must not be null'),
      check('size').isDecimal().withMessage('must be a number'),

      // fields status
      check('status').not().isEmpty().withMessage('must not be null'),
      check('status').isIn(['is_active', 'is_not_active']).withMessage('the status not taken into account'),

      // fields type
      check('type').not().isEmpty().withMessage('must not be null'),

    // fields location
    check('location').not().isEmpty().withMessage('must not be null'),

    ]

    return errors
  }
}
