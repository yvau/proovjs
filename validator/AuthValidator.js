const { check, validationResult } = require('express-validator/check');
let model = require('../models/index')

// class to validate form 
module.exports = class AuthValidator {

  /**
   * Method used validate register form.
   */
  setRegister () {
    // create strategy for the form registration
    let errors = [
    // fieds lastname
    check('lastName').not().isEmpty().withMessage('must not be null'),

    // fields credential
    check('credential').not().isEmpty().withMessage('munst not be null'),
    check('credential').isEmail().withMessage('is not email'),
    check('credential').custom(value => {
      return model.profile.findOne({
        attributes: {
          exclude: ['createdAt', 'updatedAt']
        }, where: {credential: value} }).then(user => {
        if (user) {
          return Promise.reject('E-mail already in use')
        }
      })
    }),

    // fields password
    check('password').not().isEmpty().withMessage('must not be null'),
    check('password').custom((value, { req }) => /^(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,}$/.test(value)).withMessage('pattern dont match'),

    // fields repassword
    check('rePassword').not().isEmpty().withMessage('must not be null'),
    check('rePassword').custom((value, { req }) => value === req.body.password).withMessage('pattern dont match'),
    ]

    return errors
  }

  /**
   * Method used validate recover form.
   */
  setRecover () {
    // create strategy for the form registration
    let errors = [
    // fields credential
    check('credential').not().isEmpty().withMessage('munst not be null'),
    check('credential').custom((value, { req }) => /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(value)).withMessage('is not email'),
    check('credential').isEmail().withMessage('is not email'),
    check('credential').custom(value => {
      return model.profile.findOne({
        attributes: {
          exclude: ['createdAt', 'updatedAt']
        }, where: {credential: value} }).then(user => {
        if (!user) {
          return Promise.reject('E-mail not found')
        }
      })
    }),
    ]

    return errors
  }
}
