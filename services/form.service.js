
let model = require('../models/index')

// remove duplicate errors from checking form
exports.removeDuplicates = (payload) => {
  let customArray = payload.customArray
  let prop = payload.prop
  return customArray.filter((obj, pos, arr) => {
    return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos
  })
}

// get value of last data id to increment
exports.getIncrement = async(payload) => {
  [err, increment] = await to(model.auto_increment.findOne({ where: { id: payload } }))
  if(err) {
    throwError(err.message)
  } else {
    // let increment_number = ++increment.increment_number
    await to(model.auto_increment.update({increment_number: ++increment.increment_number},
                                         {where: {id: increment.id}}))
  }
  return increment
}

// get value of last data id to increment
exports.getAutoIncrement = (payload) => {
  return model.auto_increment.find({
    where: {id: payload},
    limit: 1
  })
}

// get value of last data id to increment
exports.saveAutoIncrement = (payload) => {
  model.auto_increment.update(
    {increment_number: payload.number},
    {where: {id: payload.id}}
  ).then(autoIncrement => {
    console.log(autoIncrement)
  })
}

// generate token
exports.generateToken = () => {
  
  // set the length of the string
  let stringLength = 20

  // list containing characters for the random string
  let stringArray = ['0','1','2','3','4','5','6','7','8','9','a','b','c',
  'd','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u',
  'v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M',
  'N','O','P','Q','R','S','T','U','V','W','X','Y','Z','!','?']

  let rndString = ''
  // build a string with random characters
  for (let i = 1; i < stringLength; i++) { 
    let rndNum = Math.ceil(Math.random() * stringArray.length) - 1
    rndString = rndString + stringArray[rndNum]
  }
  
  return rndString
}