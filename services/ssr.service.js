  const fs = require('fs')
  const bundle =  require('../public/server.bundle.js')
  const manifest = require('../render_files/manifest.js')
  
  
  let vendorJs = manifest['vendor.js']
  let manifestJs = manifest['manifest.js']
  let appJs = manifest['app.js']
  let appCss = manifest['app.css']
  const template = `<!doctype html>
    <html>
     <head>
       <meta charset='utf-8' />
       <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no' />
	   {{{ meta }}}
	   <link rel="stylesheet" href="${ appCss }" />
	   <title>{{ title }}</title>
      </head>
  <body class="fixed-header horizontal-app-menu">
    <!--vue-ssr-outlet-->
   <script type="text/javascript" src="${ manifestJs }"></script>
   <script type="text/javascript" src="${ vendorJs }"></script>
   
   <script type="text/javascript" src="${ appJs }"></script>
   <script src='https://js.stripe.com/v3/'></script>
 </body>
</html>`
 
  // get renderer from vue server renderer
  const renderer = require('vue-server-renderer').createRenderer({
    // set template
    template
  })
  exports.ssr = (context, req, res, next) => {
    return bundle.default({ url: req.originalUrl }).then((app) => {    
      //context to use as data source
      //in the template for interpolation

      renderer.renderToString(app, context, function (err, html) {   
        if (err) {
          if (err.code === 404) {
            res.status(404).end('Page not found')
          } else {
            res.status(500).end('Internal Server Error')
          }
        } else {
          res.end(html)
        }
      })        
    }, (err) => {
      console.log(err)
    }) 
  }