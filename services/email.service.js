let nodemailer = require('nodemailer')
let ejs = require('ejs')
let env       = process.env.NODE_ENV || 'development'
let config    = require('../config/index')[env].mail

let transporter = nodemailer.createTransport({
  host: config.smtp,
  port: config.port,
  ignoreTLS: config.tls,
  secure: config.secure,
  auth: (process.env.NODE_ENV === 'development') ? null  : {user: config.auth.user, pass: config.auth.pass}
})

exports.sendMail = function(content, subject) {
  ejs.renderFile('views/registration.ejs', content, function (err, data) {
    if (err) {
      console.log(err)
    } else {
      let mailOptions = {
        from: '"proov app" <' + config.auth.user + '>', // sender address
        to: content.login, // list of receivers
        subject: subject, // Subject line
        html: data // html body
      }
      transporter.sendMail(mailOptions, function (err, info) {
        if (err) {
          console.log(err)
        } else {
          console.log('Message %s sent: %s', info.messageId, info.response)
        }
      })
    }      
  })
}