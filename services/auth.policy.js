// let verifyToken = require('./auth.service').verifyToken
let jwt = require('jsonwebtoken')
let model = require('../models/index')

// middleware for doing role-based permissions
exports.permit = (entity, ...roles) => {

  // return a middleware
  let istotep = async (req, res, next) => {
      let tokenToVerify
  
      /* var cookieExtractor = function(req) {
        var token = null;
        if (req && req.cookies)
        {
            token = req.cookies['jwt'];
        }
        return token;
      }; */
      
	  if (req.header('Authorization')) {
        const parts = req.header('Authorization').split(' ')

        if (parts.length === 2) {
          const scheme = parts[0]
          const credentials = parts[1]

          if (/^Bearer$/.test(scheme)) {
            tokenToVerify = credentials
          } else {
            return errorMessage(res, {message: 'Format for Authorization: Bearer [token]'}, 401)
          }
        } else {
          return errorMessage(res, {message: 'Format for Authorization: Bearer [token]'}, 401)
        }
    }
    return jwt.verify(tokenToVerify, 'secret', async function(err, decoded) {
      if (err) return errorMessage(res, {message: err}, 401)
      let error, user, user_information, user_photo
  
	  [error, res.user] = await to(model.profile.findOne({ where: { id: decoded.id }}))
	  
	  if (res.user.profile_information_id !== null) {
		[error, res.user.profile_information_id] = await to(model.profile_information.findOne({ where: { id: res.user.profile_information_id }}))
	  }

	  if (res.user.profile_information_id.profile_photo_id !== null) {
		[error, res.user.profile_information_id.profile_photo_id] = await to(model.profile_photo.findOne({ where: { id: res.user.profile_information_id.profile_photo_id }}))
	  }
      
      if(error) return errorMessage(res, {message: 'You are not authorized to view this content'}, 401)
	  
      let expirationDate = new Date(decoded.exp * 1000)
      if (expirationDate < new Date()) {
        return errorMessage(res, {message: 'Your token has expired'}, 401)
	  }
      
      if (roles.length > 0) {
        if (!user.role.split(',').some( e => roles.includes(e) )) {
          return errorMessage(res, {message: 'You are not authorized to view this content'}, 401)
        }
      }

      if (req.body.id !== undefined) {
        [err, result] = await to(model[entity].findOne({ where: {id: req.body.id} }))
        if (result.profile_id !== user.id) {
          return errorMessage(res, {message: 'You are not authorized to view this content'}, 401)
        }
      }
	  
	  // res.user = user

      return next()
    })
  }
  
  return istotep
}