let env = process.env.NODE_ENV
let config_secret = require('../config/index')[env].jwt_secret
var passport = require('passport')
let jwt = require('jsonwebtoken')
let model = require('../models/index')
let bcrypt = require('bcrypt-nodejs')

let secret = config_secret

let checkEmail = (payload) => {
  let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/
    if (!reg.test(payload)) return false
	  return true
}

let issueToken = (payload) => {
	return jwt.sign(payload, 'secret', { expiresIn: 10800 })
}

exports.login = async (payload) => { //returns token
  let token
  let user

  if(payload.password === null) throwError('Please enter a password to login')
  
  if (checkEmail(payload.credential)) {
    [err, user] = await to(model.profile.findOne({ where: { credential: payload.credential } }))
		
	  // if there is no user in database throw error
      if(err) throwError(err.message)
    } else {
	  
	  // if user email is not valid throw error
      throwError('A valid email was not entered')
  }

  // check if user is in database, if not throw error
  if(!user) throwError('Not registered')

  // check if user is in database, if not throw error
  if(!user.enabled) throwError('Not enabled')

  // check if user is in database, if not throw error
  if(!user.account_non_locked) throwError('Not locked')

  // compare user password in database with password entered
  let passwordMatch = bcrypt.compareSync(payload.password, user.password)

  // if the password don't match throw error
  if(!passwordMatch) throwError('credential is not good')
  
  const userObject = { id: user.id, login: user.credential, time: new Date() } 
  
  token = issueToken(userObject) 
  
  return token
}
