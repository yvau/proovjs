const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const profile = require('../models').profile

module.exports = function(passport){
    var opts = {}
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
    opts.secretOrKey = 'secret'

    passport.use(new JwtStrategy(opts, async function(jwt_payload, done){
        let err, user
        [err, user] = await to(profile.findOne({ where: { id: jwt_payload.id } }))

        // if(err) return done(err, false)
        if(user) {
            return done(null, user)
        } else {
            return done(null, false)
        }
    }))
}