let socket = require('socket.io')

let socketConn = (server) => {
    const io = socket.listen(server)
    io.on('connection', (socket) => {
      console.log('made socket connection', socket.id)
      // Handle chat event
      socket.on('chat', function(data){
        // console.log(data)
        io.sockets.emit('chat', data)
      })
      
      // Handle typing event
      socket.on('typing', function(data){
        socket.broadcast.emit('typing', data)
      })
      
    })
}

module.exports = {
  socketConn,
  socket
}
  