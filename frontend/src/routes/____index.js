/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file.
 */

export default [
  // Index
  {
    path: '/',
    name: 'home.index',
    component: () => import('@/pages/Home/Index'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: true
    }
  },
  // About us
  {
    path: '/about-us/',
    name: 'aboutus.index',
    component: () => import('@/pages/About/Index'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: true
    }
  },
  // Vone concept
  {
    path: '/vone-concept/',
    name: 'voneconcept.index',
    component: () => import('@/pages/Concept/Index'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: true
    }
  },
  // Advantage
  {
    path: '/vone-advantage/',
    name: 'voneadvantage.index',
    component: () => import('@/pages/Advantage/Index'),
    // Any user can see this page
    meta: {
      auth: true
    }
  },
  // Relationship
  {
    path: '/vone-relationship/',
    name: 'vonerelationship.index',
    component: () => import('@/pages/Relationship/Index'),
    // Any user can see this page
    meta: {
      auth: true
    }
  },
  // Social Blog index
  {
    path: '/social-media-blog/',
    name: 'socialmedia.index',
    component: () => import('@/pages/SocialAndBlog/Index/Index'),
    // If the user needs to be authenticated to view this page
    meta: {
      auth: true
    }
  },
  // Blog Show
  {
    path: '/blog/:id(\\d+)',
    name: 'socialmedia.show',
    component: () => import('@/pages/SocialAndBlog/Show/Index'),
    // If the user needs to be authenticated to view this page
    meta: {
      auth: true
    }
  },
  // Term of use
  {
    path: '/term-of-use/',
    name: 'termofuse',
    component: () => import('@/pages/TermOfUse/Index'),
    // If the user needs to be authenticated to view this page
    meta: {
      auth: true
    }
  },
  // Privacy of policy
  {
    path: '/privacy-policy/',
    name: 'privacypolicy',
    component: () => import('@/pages/TermOfUse/Index'),
    // If the user needs to be authenticated to view this page
    meta: {
      auth: true
    }
  },
  // Login
  {
    path: '/login/',
    name: 'login',
    component: () => import('@/pages/Login/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Login'
    }
  },
  // Register
  {
    path: '/register/',
    name: 'register',
    component: () => import('@/pages/Register/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Register'
    }
  },
  // Recover form
  {
    path: '/login/recover',
    name: 'recover.email.index',
    component: () => import('@/pages/Recover/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Recover'
    }
  },
  // Recover password
  {
    path: '/recover/password',
    name: 'recover.password.index',
    component: () => import('@/pages/Recover/Password/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Recover password'
    }
  },
  // Activate account
  {
    path: '/activate',
    name: 'activate.index',
    component: () => import('@/pages/Activate/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Activate'
    }
  },
  // Feeds
  {
    path: '/feeds/',
    name: 'feeds',
    component: () => import('@/pages/Feeds/Index'),
    // If the user needs to be authenticated to view this page
    meta: {
      auth: true,
      breadcrumb: 'Feed'
    }
  },
  // Buy
  {
    path: '/buy/',
    component: () => import('@/pages/Buy/Index/Index'),
    name: 'buy.index',
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Buying proposal'
    }
  },
  // Buying list
  {
    path: '/buy/list',
    name: 'buy.list',
    component: () => import('@/pages/Buy/_List/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'List'
    }
  },
  // Profile new rent
  {
    path: '/buy/new',
    name: 'buy.new',
    component: () => import('@/pages/Buy/_New/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'New'
    }
  },
  // Profile edit buy
  {
    path: '/buy/edit/:id(\\d+)',
    name: 'buy.edit',
    component: () => import('@/pages/Buy/_Edit/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Edit'
    }
  },
  {
    path: '/buy/overview',
    name: 'buy.overview',
    component: () => import('@/pages/Buy/_Overview/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Overview'
    }
  },
  {
    path: '/buy/:id(\\d+)',
    component: () => import('@/pages/Buy/_Show/Index'),
    meta: {
      auth: true,
      breadcrumb: 'Show'
    },
    children: [
      { path: '', name: 'buy.show.overview', component: () => import('@/pages/Buy/_Show/_Overview/Index'), meta: { auth: true } },
      { path: 'statistics', name: 'buy.show.statistics', component: () => import('@/pages/Buy/_Show/_Statistics/Index'), meta: { auth: true, breadcrumb: 'Statistics' } },
      { path: 'proposition', name: 'buy.show.propositions', component: () => import('@/pages/Buy/_Show/_Propositions/Index'), meta: { auth: true, breadcrumb: 'Propositions' } },
      { path: 'settings', name: 'buy.show.settings', component: () => import('@/pages/Buy/_Show/_Settings/Index'), meta: { auth: true, breadcrumb: 'Settings' } }
    ]
  },
  // Rent
  {
    path: '/rent/',
    component: () => import('@/pages/Rent/_Index/Index'),
    name: 'rent.index',
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Renting proposal'
    }
  },
  {
    path: '/rent/list',
    name: 'rent.list',
    component: () => import('@/pages/Rent/_List/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'List'
    }
  },
  {
    path: '/rent/new',
    name: 'rent.new',
    component: () => import('@/pages/Rent/_New/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'New'
    }
  },
  // Profile edit buy
  {
    path: '/rent/edit/:id(\\d+)',
    name: 'rent.edit',
    component: () => import('@/pages/Rent/_Edit/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Edit'
    }
  },
  {
    path: '/rent/overview',
    name: 'rent.overview',
    component: () => import('@/pages/Rent/_Overview/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Overview'
    }
  },
  {
    path: '/rent/:id(\\d+)',
    component: () => import('@/pages/Rent/_Show/Index'),
    meta: {
      auth: true,
      breadcrumb: 'Show'
    },
    children: [
      { path: '', name: 'rent.show.overview', component: () => import('@/pages/Rent/_Show/_Overview/Index'), meta: { auth: true, breadcrumb: 'Statistics' } },
      { path: 'statistics', name: 'rent.show.statistics', component: () => import('@/pages/Rent/_Show/_Statistics/Index'), meta: { auth: true, breadcrumb: 'Statistics' } },
      { path: 'proposition', name: 'rent.show.propositions', component: () => import('@/pages/Rent/_Show/_Propositions/Index'), meta: { auth: true, breadcrumb: 'Propositions' } },
      { path: 'settings', name: 'rent.show.settings', component: () => import('@/pages/Rent/_Show/_Settings/Index'), meta: { auth: true, breadcrumb: 'Settings' } }
    ]
  },
  // Property
  {
    path: '/property/',
    component: () => import('@/pages/Property/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Property'
    },
    children: [
      {
        path: 'list',
        name: 'property.list',
        component: () => import('@/pages/Property/_List/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'List'
        }
      },
      // Profile new rent
      {
        path: 'new',
        name: 'property.new',
        component: () => import('@/pages/Property/_New/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'New'
        }
      },
      // Profile edit buy
      {
        path: 'edit/:id(\\d+)',
        name: 'property.edit',
        component: () => import('@/pages/Property/_Edit/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'Edit'
        }
      },
      {
        path: ':id(\\d+)',
        component: () => import('@/pages/Property/_Show/Index'),
        meta: {
          auth: true,
          breadcrumb: 'Show'
        },
        children: [
          { path: '', name: 'property.show.overview', component: () => import('@/pages/Property/_Show/_Overview/Index'), meta: { auth: true, breadcrumb: 'Statistics' } },
          { path: 'statistics', name: 'property.show.statistics', component: () => import('@/pages/Property/_Show/_Statistics/Index'), meta: { auth: true, breadcrumb: 'Statistics' } },
          { path: 'proposition', name: 'property.show.propositions', component: () => import('@/pages/Property/_Show/_Propositions/Index'), meta: { auth: true, breadcrumb: 'Propositions' } },
          { path: 'settings', name: 'property.show.settings', component: () => import('@/pages/Property/_Show/_Settings/Index'), meta: { auth: true, breadcrumb: 'Settings' } }
        ]
      }
    ]
  },
  // Lease
  {
    path: '/lease/',
    name: 'lease.index',
    component: () => import('@/pages/Lease/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Lease'
    },
    children: [
      { path: 'overview', name: 'lease.overview', component: () => import('@/pages/Lease/_Overview/Index'), meta: { auth: true, breadcrumb: 'overview' } },
      { path: 'new', name: 'lease.new', component: () => import('@/pages/Lease/_New/Index'), meta: { auth: true, breadcrumb: 'new leasing' } }
    ]
  },
  // profile new rent
  {
    path: '/profile/',
    component: () => import('@/pages/Profile/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'profile'
    },
    children: [
      // Profile show
      {
        path: '/:id',
        name: 'profile.show',
        component: () => import('@/pages/Profile/Get/Information/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'Get information'
        }
      },
      // Profile get_information
      {
        path: '/profile/get_information',
        name: 'profile.getinformation',
        component: () => import('@/pages/Profile/Get/Information/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'Get information'
        }
      },
      {
        path: 'edit',
        meta: {
          breadcrumb: 'edit'
        },
        component: () => import('@/pages/Profile/_Edit/Index'),
        children: [
          { path: '', name: 'profile.edit', component: () => import('@/pages/Profile/_Edit/_Index/Index'), meta: { auth: true } },
          { path: 'password', name: 'profile.edit.password', component: () => import('@/pages/Profile/_Edit/_Password/Index'), meta: { auth: true, breadcrumb: 'Password' } },
          { path: 'contact', name: 'profile.edit.contact', component: () => import('@/pages/Profile/_Edit/_Contact/Index'), meta: { auth: true, breadcrumb: 'Contact' } },
          { path: 'role', name: 'profile.edit.role', component: () => import('@/pages/Profile/_Edit/_Role/Index'), meta: { auth: true, breadcrumb: 'Role' } },
          { path: 'information', name: 'profile.edit.information', component: () => import('@/pages/Profile/_Edit/_Information/Index'), meta: { auth: true, breadcrumb: 'Information' } }
        ]
      },
      // profile show own
      {
        path: '',
        name: 'profile.own',
        component: () => import('@/pages/Profile/_Index/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'buying proposal overview'
        }
      }
    ]
  },
  // Bookmarks
  {
    path: '/bookmarks/',
    name: 'bookmarks.index',
    component: () => import('@/pages/Bookmarks/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      auth: true,
      breadcrumb: 'Bookmarks'
    }
  },

  // Notifications
  {
    path: '/notifications/',
    name: 'notification.index',
    component: () => import('@/pages/Notification/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      auth: true,
      breadcrumb: 'Notifications'
    }
  },

  // Account
  {
    path: '/account/',
    name: 'account.index',
    component: () => import('@/pages/Account/Index'),

    // If the user needs to be authenticated to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Account'
    }
  },

  // Account Show
  {
    path: '/account/:id(\\d+)',
    name: 'account.showx',
    component: () => import('@/pages/Account/Show/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Show'
    }
  },

  // Account List
  {
    path: '/account/list',
    name: 'accountList.index',
    component: () => import('@/pages/Account/List/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Bar Page'
    }
  },

  // Sell
  {
    path: '/sell/',
    name: 'sell.index',
    component: () => import('@/pages/Sell/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Sell'
    }
  },

  // Profile new property
  {
    path: '/sell/new',
    name: 'sell.new',
    component: () => import('@/pages/Profile/Sell/New/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Selling form'
    }
  },

  // Profile edit information
  {
    path: '/payment/paypal',
    name: 'paymentPaypal.index',
    component: () => import('@/pages/Payment/Paypal/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'payment paypal'
    }
  },

  // Profile edit information
  {
    path: '/payment/card',
    name: 'paymentCard.index',
    component: () => import('@/pages/Payment/Card/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Card'
    }
  },

  // Login
  {
    path: '/login',
    name: 'login.index',
    component: () => import('@/pages/Login/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: true
    }
  },

  // Register
  {
    path: '/register',
    name: 'register.index',
    component: () => import('@/pages/Register/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: true
    }
  },

  {
    path: '/',
    redirect: '/home'
  },

  {
    path: '/*',
    redirect: '/home'
  }
]
