/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file.
 */

export default [
  // Home
  {
    path: '/home',
    name: 'home.index',
    component: () => import('@/pages/Home/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      auth: false
    }
  },

  // Notifications
  {
    path: '/notifications/',
    name: 'notification.index',
    component: () => import('@/pages/Notification/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      auth: true,
      breadcrumb: 'Notifications'
    }
  },

  // Account
  {
    path: '/account',
    name: 'account.index',
    component: () => import('@/pages/Account/Index'),

    // If the user needs to be authenticated to view this page.
    meta: {
      auth: true
    }
  },

  // Account Show
  {
    path: '/account/:id(\\d+)',
    component: () => import('@/pages/Account/Show/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: true
    },
    children: [
      { path: '', name: 'account.show.index', component: () => import('@/pages/Account/Show/_Overview/Index'), meta: { auth: true } },
      { path: 'activities', name: 'account.show.activity', component: () => import('@/pages/Account/Show/_Activity/Index'), meta: { auth: true } },
      { path: 'buy', name: 'account.show.buy', component: () => import('@/pages/Account/Show/_Buy/Index'), meta: { auth: true } },
      { path: 'rent', name: 'account.show.rent', component: () => import('@/pages/Account/Show/_Rent/Index'), meta: { auth: true } },
      { path: 'properties', name: 'account.show.property', component: () => import('@/pages/Account/Show/_Property/Index'), meta: { auth: true } },
      { path: 'reviews', name: 'account.show.review', component: () => import('@/pages/Account/Show/_Review/Index'), meta: { auth: true } }
    ]
  },

  // Property
  {
    path: '/property/',
    component: () => import('@/pages/Property/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Property'
    },
    children: [
      {
        path: 'list',
        name: 'property.list',
        component: () => import('@/pages/Property/_List/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'List'
        }
      },
      // Profile new rent
      {
        path: 'new',
        name: 'property.new',
        component: () => import('@/pages/Property/_New/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'New'
        }
      },
      // Profile edit buy
      {
        path: 'edit/:id(\\d+)',
        name: 'property.edit',
        component: () => import('@/pages/Property/_Edit/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'Edit'
        }
      },
      {
        path: ':id(\\d+)',
        component: () => import('@/pages/Property/_Show/Index'),
        meta: {
          auth: true,
          breadcrumb: 'Show'
        },
        children: [
          { path: '', name: 'property.show.overview', component: () => import('@/pages/Property/_Show/_Overview/Index'), meta: { auth: true, breadcrumb: 'Statistics' } },
          { path: 'statistics', name: 'property.show.statistics', component: () => import('@/pages/Property/_Show/_Statistics/Index'), meta: { auth: true, breadcrumb: 'Statistics' } },
          { path: 'proposition', name: 'property.show.propositions', component: () => import('@/pages/Property/_Show/_Propositions/Index'), meta: { auth: true, breadcrumb: 'Propositions' } },
          { path: 'settings', name: 'property.show.settings', component: () => import('@/pages/Property/_Show/_Settings/Index'), meta: { auth: true, breadcrumb: 'Settings' } }
        ]
      }
    ]
  },

  // Feeds
  {
    path: '/feeds/',
    name: 'feeds',
    component: () => import('@/pages/Feeds/Index'),
    // If the user needs to be authenticated to view this page
    meta: {
      auth: true,
      breadcrumb: 'Feed'
    }
  },

  // Login
  {
    path: '/login',
    name: 'login.index',
    component: () => import('@/pages/Login/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: false
    }
  },

  // Register
  {
    path: '/register',
    name: 'register.index',
    component: () => import('@/pages/Register/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: false
    }
  },

  // About us
  {
    path: '/about-us/',
    name: 'aboutus.index',
    component: () => import('@/pages/About/Index.vue'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  // Vone advantage
  {
    path: '/vone-advantage/',
    name: 'voneadvantage.index',
    component: () => import('@/pages/Advantage/Index.vue'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  // Vone relationship
  {
    path: '/vone-relationship/',
    name: 'vonerelationship.index',
    component: () => import('@/pages/Relationship/Index.vue'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  // Vone relationship
  {
    path: '/social-media/',
    name: 'socialmedia.index',
    component: () => import('@/pages/SocialAndBlog/Index/Index.vue'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  // Vone concept
  {
    path: '/vone-concept/',
    name: 'voneconcept.index',
    component: () => import('@/pages/Concept/Index.vue'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  // Buy
  {
    path: '/buy/',
    component: () => import('@/pages/Buy/_Index/Index'),
    name: 'buy.index',
    // If the user needs to be a guest to view this page.
    meta: {
      auth: false,
      breadcrumb: 'Buying proposal'
    }
  },

  {
    path: '/buy/overview',
    name: 'buy.overview',
    component: () => import('@/pages/Buy/_Overview/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Overview'
    }
  },

  // Buying list
  {
    path: '/buy/list',
    name: 'buy.list',
    component: () => import('@/pages/Buy/_List/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'List'
    }
  },
  // Profile new rent
  {
    path: '/buy/new',
    name: 'buy.new',
    component: () => import('@/pages/Buy/_New/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'New'
    }
  },

  {
    path: '/buy/:id(\\d+)',
    component: () => import('@/pages/Buy/Show/Index'),
    meta: {
      auth: true,
      breadcrumb: 'Show'
    },
    children: [
      { path: '', name: 'buy.show.overview', component: () => import('@/pages/Buy/Show/_Overview/Index'), meta: { auth: true } },
      { path: 'statistics', name: 'buy.show.statistics', component: () => import('@/pages/Buy/Show/_Statistics/Index'), meta: { auth: true, breadcrumb: 'Statistics' } },
      { path: 'proposition', name: 'buy.show.propositions', component: () => import('@/pages/Buy/Show/_Propositions/Index'), meta: { auth: true, breadcrumb: 'Propositions' } },
      { path: 'settings', name: 'buy.show.settings', component: () => import('@/pages/Buy/Show/_Settings/Index'), meta: { auth: true, breadcrumb: 'Settings' } }
    ]
  },

  // Rent
  {
    path: '/rent/',
    component: () => import('@/pages/Rent/_Index/Index'),
    name: 'rent.index',
    // If the user needs to be a guest to view this page.
    meta: {
      auth: false
    }
  },

  {
    path: '/rent/overview',
    name: 'rent.overview',
    component: () => import('@/pages/Rent/_Overview/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Overview'
    }
  },

  {
    path: '/rent/list',
    name: 'rent.list',
    component: () => import('@/pages/Rent/_List/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'List'
    }
  },

  {
    path: '/rent/new',
    name: 'rent.new',
    component: () => import('@/pages/Rent/_New/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'New'
    }
  },

  {
    path: '/rent/:id(\\d+)',
    component: () => import('@/pages/Rent/_Show/Index'),
    meta: {
      auth: true,
      breadcrumb: 'Show'
    },
    children: [
      { path: '', name: 'rent.show.overview', component: () => import('@/pages/Rent/_Show/_Overview/Index'), meta: { auth: true, breadcrumb: 'Statistics' } },
      { path: 'statistics', name: 'rent.show.statistics', component: () => import('@/pages/Rent/_Show/_Statistics/Index'), meta: { auth: true, breadcrumb: 'Statistics' } },
      { path: 'proposition', name: 'rent.show.propositions', component: () => import('@/pages/Rent/_Show/_Propositions/Index'), meta: { auth: true, breadcrumb: 'Propositions' } },
      { path: 'settings', name: 'rent.show.settings', component: () => import('@/pages/Rent/_Show/_Settings/Index'), meta: { auth: true, breadcrumb: 'Settings' } }
    ]
  },

  // Sell
  {
    path: '/sell/',
    name: 'sell.index',
    component: () => import('@/pages/Sell/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: false
    }
  },

  // Lease
  {
    path: '/lease/',
    name: 'lease.index',
    component: () => import('@/pages/Lease/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Lease'
    },
    children: [
      { path: 'overview', name: 'lease.overview', component: () => import('@/pages/Lease/_Overview/Index'), meta: { auth: true, breadcrumb: 'overview' } },
      { path: 'new', name: 'lease.new', component: () => import('@/pages/Lease/_New/Index'), meta: { auth: true, breadcrumb: 'new leasing' } }
    ]
  },

  // profile new rent
  {
    path: '/profile/',
    component: () => import('@/pages/Profile/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'profile'
    },
    children: [
      // Profile show
      {
        path: '/:id',
        name: 'profile.show',
        component: () => import('@/pages/Profile/Get/Information/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'Get information'
        }
      },
      // Profile get_information
      {
        path: '/profile/get_information',
        name: 'profile.getinformation',
        component: () => import('@/pages/Profile/Get/Information/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'Get information'
        }
      },
      {
        path: 'edit',
        meta: {
          breadcrumb: 'edit'
        },
        component: () => import('@/pages/Profile/_Edit/Index'),
        children: [
          { path: '', name: 'profile.edit', component: () => import('@/pages/Profile/_Edit/_Index/Index'), meta: { auth: true } },
          { path: 'password', name: 'profile.edit.password', component: () => import('@/pages/Profile/_Edit/_Password/Index'), meta: { auth: true, breadcrumb: 'Password' } },
          { path: 'contact', name: 'profile.edit.contact', component: () => import('@/pages/Profile/_Edit/_Contact/Index'), meta: { auth: true, breadcrumb: 'Contact' } },
          { path: 'role', name: 'profile.edit.role', component: () => import('@/pages/Profile/_Edit/_Role/Index'), meta: { auth: true, breadcrumb: 'Role' } },
          { path: 'information', name: 'profile.edit.information', component: () => import('@/pages/Profile/_Edit/_Information/Index'), meta: { auth: true, breadcrumb: 'Information' } }
        ]
      },
      // profile show own
      {
        path: '',
        name: 'profile.own',
        component: () => import('@/pages/Profile/_Index/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'buying proposal overview'
        }
      }
    ]
  },

  {
    path: '/',
    redirect: '/home'
  },

  {
    path: '/*',
    redirect: '/home'
  }
]
