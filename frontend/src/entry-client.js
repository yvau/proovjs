import { createApp } from './main'

// client-specific bootstrapping logic...
import './plugins/vue-component'
import './plugins/stylesheet'
import './plugins/multiselect'
import './plugins/jquery'
import './plugins/pace'
import './plugins/simple-vue-validator'

const { app } = createApp()

// this assumes App.vue template root element has `id="app"`
app.$mount('#app')
