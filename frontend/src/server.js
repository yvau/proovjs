// to add to window
import awaitServer from './awaitServer'
global.renderView = function (_viewName, model, url) {
const { createBundleRenderer } = require('vue-server-renderer')

const context = {
  title: 'Vue HN 2.0', // default title
  url: '/'
}
let inWait = awaitServer(function (done) {
  createBundleRenderer(bundle).renderToString(context, (err, html) => { 
    done(err, html)
  })
})

console.log(inWait.err)
console.log(inWait.error)
console.log(inWait.result)

function populateTemplate (markup) {
    return `<!doctype html>
    <html>
     <head>
       <meta charset='utf-8' />
       <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no' />
      </head>
  <body>
    test
   <script src='https://js.stripe.com/v3/'></script>
 </body>
</html>`
  }

  return populateTemplate(bundle)

}
