/* ============
 * Proposal Transformer
 * ============
 *
 * The transformer for the proposal.
 */

import Transformer from './Transformer'
import Util from '@/utils/index'

export default class ProposalTransformer extends Transformer {
  /**
   * Method used to transform a fetched proposal.
   *
   * @param proposal The fetched proposal.
   *
   * @returns {Object} The transformed proposal.
   */
  static fetch (proposal) {
    return {
      id: proposal.content.id,
      // profile: proposal.profile,
      isFurnished: Util.getFurnished(proposal.content.is_furnished),
      isUrgent: Util.getUrgent(proposal.content.is_urgent),
      priceMinimum: proposal.content.price_minimum,
      priceMaximum: proposal.content.price_maximum,
      bedrooms: proposal.content.bedrooms,
      bathrooms: proposal.content.bathrooms,
      // location: proposal.locations.map(function (x) { return x.city }),
      status: proposal.content.status,
      dateOfCreation: proposal.content.date_of_creation,
      size: proposal.content.size,
      ageOfProperty: proposal.content.age_of_property,
      // typeOfProperty: proposal.typeOfProperty.split(','),
      typeOfProposal: proposal.content.type_of_proposal
    }
  }

  /**
   * Method used to transform a send proposal.
   *
   * @param proposal The proposal to be send.
   *
   * @returns {Object} The transformed proposal.
   */
  static send (proposal) {
    console.log(proposal.location)
    return {
      urgent: Util.setUrgent(proposal.urgent),
      isFurnished: Util.setFurnished(proposal.isFurnished),
      bathrooms: proposal.bathrooms,
      bedrooms: proposal.bedrooms,
      location: (proposal.location === null) ? null : proposal.location.map(function (x) { return x.id }).toString(),
      typeOfProposal: proposal.typeOfProposal,
      status: proposal.status,
      ageOfProperty: proposal.ageOfProperty,
      priceMinimum: proposal.priceMinimum,
      priceMaximum: proposal.priceMaximum,
      size: proposal.size,
      typeOfProperty: proposal.typeOfProperty.toString()
    }
  }
}
