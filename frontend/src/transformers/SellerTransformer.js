/* ============
 * Proposal Transformer
 * ============
 *
 * The transformer for the proposal.
 */

import Transformer from './Transformer'
// import Util from 'utils/index'

export default class SellerTransformer extends Transformer {
  /**
   * Method used to transform a fetched proposal.
   *
   * @param seller The fetched proposal.
   *
   * @returns {Object} The transformed proposal.
   */
  static fetch (seller) {
    return {
      /* id: proposal.id,
      profile: proposal.profile,
      isFurnished: Util.getFurnished(proposal.isFurnished),
      isUrgent: Util.getUrgent(proposal.isUrgent),
      priceMinimum: proposal.priceMinimum,
      priceMaximum: proposal.priceMaximum,
      bedrooms: proposal.bedrooms,
      bathrooms: proposal.bathrooms,
      location: proposal.locations.map(function (x) { return x.city }),
      status: proposal.status,
      dateOfCreation: proposal.dateOfCreation,
      size: proposal.size,
      ageOfProperty: proposal.ageOfProperty,
      typeOfProperty: proposal.typeOfProperty.split(','),
      typeOfProposal: proposal.typeOfProposal */
    }
  }

  /**
   * Method used to transform a send proposal.
   *
   * @param seller to be send.
   *
   * @returns {Object} The transformed seller.
   */
  static send (seller) {
    return {
      /* urgent: Util.setUrgent(proposal.urgent),
      isFurnished: Util.setFurnished(proposal.isFurnished),
      bathrooms: proposal.bathrooms,
      bedrooms: proposal.bedrooms,
      location: (proposal.location === null) ? null : proposal.location.map(function (x) { return x.id }).toString(),
      typeOfProposal: proposal.typeOfProposal,
      status: proposal.status,
      ageOfProperty: proposal.ageOfProperty,
      priceMinimum: proposal.priceMinimum,
      priceMaximum: proposal.priceMaximum,
      size: proposal.size,
      typeOfProperty: proposal.typeOfProperty.toString() */
    }
  }
}
