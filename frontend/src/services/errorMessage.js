export default (payload) => {
  /*
   * We change the value of the errors messages
   */
  let $form = window.$('form')
  $form.find('div').removeClass('has-error')
  $form.find('.error').empty()
  for (let i = 0; i < payload.error.messageList.length; i++) {
    let item = payload.error.messageList[i]
    let $controlGroup = window.$('#' + item.param.toString().replace(/\./g, ''))
    $controlGroup.addClass('has-error')
    $controlGroup.find('.error').html(item.msg)
  }
}
