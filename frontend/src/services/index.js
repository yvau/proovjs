import errorMessage from './errorMessage'
import removeMessage from './removeMessage'

export default {
  errorMessage,
  removeMessage
}
