const LocalStorageFallback = () => {
    try {
      localStorage.setItem('isLocalStorage', 'true');
      localStorage.removeItem('isLocalStorage');
      this._storage = localStorage;
    } catch (e) {
      this._storage = {};
      this._storage.getItem = this._getItem.bind(this);
      this._storage.setItem = this._setItem.bind(this);
      this._storage.removeItem = this._removeItem.bind(this);
      this._storage.clear = this._clear.bind(this);
    }
   
    this.get = (key) => {
      let value = this._storage.getItem(key);
      if (!value) return;
      if (value[0] === '{' || value[0] === '[') {
        value = JSON.parse(value);
      }
      return value; // eslint-disable-line
    };
   
    this.set = (key, value) => {
      if (!key || !value) return;
      if (typeof value === 'object') {
        value = JSON.stringify(value); // eslint-disable-line
      }
      this._storage.setItem(key, value);
    };
   
    this.remove = (key) => {
      this._storage.removeItem(key);
    };
   
    this.clear = () => {
      this._storage.clear();
    };
   
    this._getItem = key => this._storage[key];
    this._setItem = (key, value) => {
      this._storage[key] = value;
    };
    this._removeItem = (key) => {
      delete this._storage[key];
    };
    this._clear = () => {
      delete this._storage;
      this._storage = {};
    };
  };
   
  export const localStorageHelper = new LocalStorageFallback();