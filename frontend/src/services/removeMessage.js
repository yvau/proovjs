export default () => {
  /*
   * We change the value of the errors messages
   */

  let $form = window.$('form')
  $form.find('div').removeClass('has-error')
  $form.find('.error').empty()
}
