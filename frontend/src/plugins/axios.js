/* ============
 * Axios
 * ============
 *
 * Promise based HTTP client for the browser and node.js.
 * Because Vue Resource has been retired, Axios will now been used
 * to perform AJAX-requests.
 *
 * https://github.com/mzabriskie/axios
 */

import Vue from 'vue'
import Axios from 'axios'
import store from '@/store'

Axios.defaults.baseURL = (process.env.NODE_ENV === 'development') ? 'http://localhost:3000/server' : ''
Axios.defaults.headers.common.Accept = 'application/json'
Axios.defaults.headers['X-Requested-With'] = 'XMLHttpRequest'
Axios.interceptors.response.use(
  response => response,
  (error) => {
    if (error.response !== undefined) {
      if (error.response.status === 401) {
        store.dispatch('auth/logout')
      }
    }

    return Promise.reject(error)
  })

// Bind Axios to Vue.
Vue.$http = Axios
Object.defineProperty(Vue.prototype, '$http', {
  get () {
    return Axios
  }
})
