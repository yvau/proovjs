/* ============
 * jQuery
 * ============
 *
 * Require jQuery
 *
 * http://jquery.com/
 */
import jQuery from 'jquery-slim'

window.$ = window.jQuery = jQuery

/* window.$(document).ready(function () {
  window.$('.dropdown').click(function (e) {
    window.$(this).find('.dropdown-menu').toggleClass('open')
    window.$(window.$(e.target).find('.down-caret').toggleClass('open-caret'))
    e.preventDefault()
    e.stopPropagation()
    window.$(document).click(function () {
      window.$('.dropdown-menu').removeClass('open')
      window.$('.down-caret').removeClass('open-caret')
    })
  })
}) */
