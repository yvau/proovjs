/* ============
 * Pace-js
 * ============
 *
 * An automatic web page progress bar.
 *
 * http://github.hubspot.com/pace/
 */
import 'pace-js/themes/blue/pace-theme-flash.css'
import pace from 'pace-js'

let paceOptions = {
  ajax: false
}
pace.start(paceOptions)
