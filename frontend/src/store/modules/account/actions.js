/* ============
 * Actions for the account module
 * ============
 *
 * The actions that are available on the
 * account module.
 */

import Transformer from '@/transformers/AccountTransformer'
import * as types from './mutation-types'
import Proxy from '@/proxies/AccountProxy'

export const find = ({ commit }) => {
  new Proxy()
    .find('get')
    .then((response) => {
      commit(types.FIND, Transformer.fetch(response.message))
    }).catch(() => {
      console.log('Request failed...')
    })

  // commit(types.FIND, account)
}

export default {
  find
}
