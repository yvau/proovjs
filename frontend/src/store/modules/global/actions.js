/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import store from '@/store/index'
import Proxy from '@/proxies/Proxy'
import service from '@/services/index'
import * as types from './mutation-types'

export const submit = ({ commit }, payload) => {
  let button = payload.button
  if (button !== undefined) {
    button.addClass('disabled sending')
  }
  new Proxy()
    .submit(payload.method, payload.url, payload.data)
    .then((response) => {
      if (button !== undefined) {
        button.removeClass('disabled sending')
      }
      try {
        service.errorMessage(response)
      } catch (err) {
        store.dispatch('alert/message', {message: response.error, status: true, success: response.success})
      }
    })
    .catch(() => {
      if (button !== undefined) {
        button.removeClass('disabled sending')
      }
      console.log('Request failed...')
    })
}

export const setObject = ({ commit }, payload) => {
  commit(types.OBJECT, payload)
}

export const setList = ({ commit }, payload) => {
  commit(types.LIST, payload)
}

export const find = ({ commit }, payload) => {
  new Proxy(payload.url, {})
    .find(payload.data)
    .then((response) => {
      try {
        commit(types.OBJECT, payload.transformer.fetch(response))
      } catch (err) {
        console.log(err)
      }
    })
    .catch(() => {
      console.log('Request failed...')
    })
}

export const findAll = ({ commit }, payload) => {
  commit(types.LOADING, true)
  new Proxy(payload.url, {})
    .setParameters(payload.parameters)
    .all()
    .then((response) => {
      try {
        commit(types.LIST, response)
        commit(types.LOADING, false)
      } catch (err) {
        console.log(err)
      }
    })
    .catch(() => {
      commit(types.LOADING, false)
      console.log('Request failed...')
    })
}

export const loading = ({ commit }, payload) => {
  commit(types.LOADING, payload)
}

export default {
  submit,
  findAll,
  loading,
  setObject,
  setList,
  find
}
