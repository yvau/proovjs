/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import Axios from 'axios'
import storage from 'local-storage-fallback'
import {
  CHECK,
  LOGIN,
  LOGOUT
} from './mutation-types'

export default {
  [CHECK] (state) {
    state.authenticated = !!storage.getItem('AUTH-ID')
    if (state.authenticated) {
      Axios.defaults.headers.common['Authorization'] = `Bearer ${storage.getItem('AUTH-ID')}`
    }
  },

  [LOGIN] (state, payload) {
    state.authenticated = true
    storage.setItem('AUTH-ID', payload.message)
    Axios.defaults.headers.common['Authorization'] = `Bearer ${payload.message}`
  },

  [LOGOUT] (state) {
    state.authenticated = false
    storage.removeItem('AUTH-ID')
    Axios.defaults.headers.common['Authorization'] = ''
  }
}
