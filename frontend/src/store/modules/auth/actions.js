/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import Vue from 'vue'
import store from '@/store/index'
import * as types from './mutation-types'
import Proxy from '@/proxies/AuthProxy'
import service from '@/services/index'

export const check = ({ commit }) => {
  commit(types.CHECK)
}

// merge to submit global (form/create)
export const register = ({ commit }, payload) => {
  let button = window.$('#form-register')
  button.addClass('disabled sending')
  new Proxy()
    .register(payload)
    .then((response) => {
      try {
        service.errorMessage(response)
      } catch (err) {
        store.dispatch('alert/message', {message: response.message, status: true, success: response.success})
      }
      button.removeClass('disabled sending')
    })
    .catch(() => {
      button.removeClass('disabled sending')
      console.log('Request failed...')
    })
}

export const login = ({ commit }, payload) => {
  let button = window.$('#form-login')
  button.addClass('disabled sending')
  new Proxy()
    .login(payload)
    .then((response) => {
      if (response.success) {
        console.log(response)
        commit(types.LOGIN, response)
        // window.location.replace('/')
        Vue.router.push({
          name: 'home.index'
        })
      } else {
        store.dispatch('alert/message', {message: response.error, status: true, success: response.success})
      }
      button.removeClass('disabled sending')
    })
    .catch(() => {
      button.removeClass('disabled sending')
      console.log('Request failed...')
    })
}

export const activate = ({ commit }, payload) => {
  new Proxy().setParameter('t', payload).find('activate')
    .then((response) => {
      if (response.success) {
        commit(types.LOGIN, response.message)
        // store.dispatch('account/find')
        Vue.router.push({
          name: 'home.index'
        })
      } else {
        store.dispatch('alert/message', {message: response.error, status: true, success: response.success})
      }
    })
    .catch((err) => {
      console.log(err)
    })

  /* Vue.router.push({
    name: 'home.index'
  }) */
}

export const recover = ({ commit }, payload) => {
  new Proxy().setParameter('token', payload).find('recover')
    .then((response) => {
      if (response.field !== 'Success') {
        store.dispatch('alert/message', {message: response.error, status: true, success: response.success})
      }
      // commit(types.LOGIN, response)
      // store.dispatch('account/find')
      /* Vue.router.push({
        name: 'home.index'
      }) */
    })
    .catch(() => {

    })

  /* Vue.router.push({
    name: 'home.index'
  }) */
}

export const logout = ({ commit }) => {
  commit(types.LOGOUT)
  Vue.router.push({
    name: 'login.index'
  })
}

export default {
  check,
  register,
  login,
  activate,
  recover,
  logout
}
