export default (payload) => {
  /*
   * We change the value of the furnised
   */
  let furnished = {
    false: 'not_furnished',
    true: 'is_furnished',
    'default': ''
  }
  return (furnished[payload] || furnished['default'])
}
