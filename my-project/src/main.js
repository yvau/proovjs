/* ============
 * Main File
 * ============
 *
 * Will initialize the application.
 */

import Vue from 'vue'

/* ============
 * Plugins
 * ============
 *
 * Import and bootstrap the plugins.
 */

import './plugins/vuex'
import { i18n } from './plugins/vue-i18n'
import { router } from './plugins/vue-router'
import './plugins/vuex-router-sync'
import './plugins/axios'

/* ============
 * Main App
 * ============
 *
 * Last but not least, we import the main application.
 */

import App from './App'
import store from './store'

Vue.config.productionTip = false

store.dispatch('auth/check')

/* eslint-disable no-new */
export function createApp () {
  const app = new Vue({
    /**
     * The localization plugin.
     */
    i18n,

    /**
     * The router.
     */
    router,

    /**
     * The Vuex store.
     */
    store,

    /**
     * Will render the application.
     *
     * @param {Function} h Will create an element.
     */
    render: h => h(App)
  })
  return { app, router, store }
}
