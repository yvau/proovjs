/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file.
 */

export default [
  // Home
  {
    path: '/home',
    name: 'home.index',
    component: () => import('@/pages/Home/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      auth: false
    }
  },

  // Account
  {
    path: '/account',
    name: 'account.index',
    component: () => import('@/pages/Account/Index'),

    // If the user needs to be authenticated to view this page.
    meta: {
      auth: true
    }
  },

  // Login
  {
    path: '/login',
    name: 'login.index',
    component: () => import('@/pages/Login/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: false
    }
  },

  // Register
  {
    path: '/register',
    name: 'register.index',
    component: () => import('@/pages/Register/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: false
    }
  },

  // About us
  {
    path: '/about-us/',
    name: 'aboutus.index',
    component: () => import('@/pages/About/Index.vue'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  // Vone advantage
  {
    path: '/vone-advantage/',
    name: 'voneadvantage.index',
    component: () => import('@/pages/Advantage/Index.vue'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  // Vone relationship
  {
    path: '/vone-relationship/',
    name: 'vonerelationship.index',
    component: () => import('@/pages/Relationship/Index.vue'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  // Vone relationship
  {
    path: '/social-media/',
    name: 'socialmedia.index',
    component: () => import('@/pages/SocialAndBlog/Index/Index.vue'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  // Vone concept
  {
    path: '/vone-concept/',
    name: 'voneconcept.index',
    component: () => import('@/pages/Concept/Index.vue'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  {
    path: '/',
    redirect: '/home'
  },

  {
    path: '/*',
    redirect: '/home'
  }
]
