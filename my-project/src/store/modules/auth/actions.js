/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import Vue from 'vue'
import store from '@/store'
import * as types from './mutation-types'
import Proxy from '@/proxies/AuthProxy'
import service from '@/services/index'

export const check = ({ commit }) => {
  commit(types.CHECK)
}

export const register = ({ commit }, payload) => {
  let button = window.$('#form-register')
  button.addClass('loading')
  new Proxy()
    .register(payload)
    .then((response) => {
      try {
        service.errorMessage(response)
      } catch (err) {
        store.dispatch('alert/message', {message: response.error, status: true, success: response.success})
      }
      button.removeClass('loading')
    })
    .catch(() => {
      button.removeClass('loading')
      console.log('Request failed...')
    })
}

export const login = ({ commit }, payload) => {
  let button = window.$('#form-login')
  button.addClass('loading')
  new Proxy()
    .login(payload)
    .then((response) => {
      if (response.success) {
        commit(types.LOGIN, response)
        store.dispatch('account/find')
        Vue.router.push({
          name: 'home.index'
        })
      } else {
        store.dispatch('alert/message', {message: response.error, status: true, success: response.success})
      }
      button.removeClass('loading')
    })
    .catch(() => {
      button.removeClass('loading')
      console.log('Request failed...')
    })
}

export const logout = ({ commit }) => {
  commit(types.LOGOUT)
  Vue.router.push({
    name: 'login.index'
  })
}

export default {
  check,
  register,
  login,
  logout
}
