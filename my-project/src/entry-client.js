import { createApp } from './main'

// client-specific bootstrapping logic...
import './plugins/stylesheet'
import './plugins/font-awesome'
import './plugins/vue-component'
import './plugins/jquery'
import './plugins/multiselect'

const { app } = createApp()

// this assumes App.vue template root element has `id="app"`
app.$mount('#app')
