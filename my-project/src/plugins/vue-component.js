import Vue from 'vue'

Vue.component('message-alert', () => import('@/components/message/Alert'))
Vue.component('modal-forgot-password', () => import('@/components/modal/ModalForgotPassword'))
Vue.component('menu-layout', () => import('@/components/Menu'))
