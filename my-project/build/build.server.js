'use strict'
require('./check-versions')()

process.env.NODE_ENV = 'production'

const ora = require('ora')
const rm = require('rimraf')
const path = require('path')
const chalk = require('chalk')
const webpack = require('webpack')
const shell = require('shelljs')
const webpackConfig = require('./webpack.server.conf')

const spinner = ora('building server for production...')
spinner.start()

  webpack(webpackConfig, function (err, stats) {
    spinner.stop()
    if (err) throw err
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false
    }) + '\n\n')

    shell.rm('-rf', '../public/')
    shell.cp('-R', 'dist/', '../public/')
    shell.cp('-R', 'favicon.ico', '../public/favicon.ico')

    if (stats.hasErrors()) {
      console.log(chalk.red('  Build failed with errors.\n'))
      process.exit(1)
    }

    console.log(chalk.cyan('  Build complete.\n'))
    console.log(chalk.yellow('  Server chunk file ready.\n'))
  })

