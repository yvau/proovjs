var express = require('express')
var router = express.Router()
var model = require('../models/index')
let validator  = require('../validator/ProposalValidator')
const { validationResult } = require('express-validator/check')
const formService = require('../services/form.service')
const auth = require('../services/auth.service')
const filterQuery = require('../filter/FilterQueryProposal')
var maxmind = require('maxmind')

/* GET proposal show. */
router.get('/proposal/:id', function(req, res, next) {
  model.proposal.belongsToMany(model.location, {through: model.proposal_has_location, foreignKey: 'proposal_id', constraints: false})
  model.location.belongsToMany(model.proposal, {through: model.proposal_has_location,  foreignKey: 'location_id', constraints: false})

  // query to load data from entity
  model.proposal.findOne({
    where: {id: req.params.id},
    include: [ { model: model.location } ]
  }).then(function(entity) {
    
    // render data from query
    res.json({'content': entity})
  })
})


/* GET proposals list. */
router.get('/proposals/list', async function(req, res, next) {
  model.proposal.belongsToMany(model.location, {through: model.proposal_has_location, foreignKey: 'proposal_id', constraints: false})
  model.location.belongsToMany(model.proposal, {through: model.proposal_has_location,  foreignKey: 'location_id', constraints: false})
  model.location.belongsTo(model.city, {foreignKey: 'city_id'})
  // init limitation row according to page
  let limit = (req.query.size === undefined) ? 8 : req.query.size
  let offset = (req.query.page === undefined) ? 0 : limit * (req.query.page - 1)
  
  // lookup for city object according to ip address
  let ip = (process.env.NODE_ENV === 'development') ? '66.6.44.4' : req.headers['x-forwarded-for'] || req.connection.remoteAddress
  
  // query to load data from entity
  model.proposal.findAndCountAll({
    where: filterQuery.fetch(req.query),
    include: [ { model: model.location, required: true,
      include: [
        {model: model.city}
      ] 
    } ],
    order: [
      ['id', 'DESC']
    ],
    limit: limit,
    offset: offset
  }).then(function(entity) {
    
    // render data from query
    maxmind.open('./bin/GeoLite2-City.mmdb', (err, cityLookup) => {
      var organization = cityLookup.get(ip)
      res.json({'content': entity.rows, 'count': entity.count, 'variable': organization.city.names['en']})
    });
    
  })
})

module.exports = router
