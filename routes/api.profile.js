var express = require('express')
var router = express.Router()
var model = require('../models/index')
let validator  = require('../validator/ProfileValidator')
const permit = require('../services/auth.policy').permit
const { validationResult } = require('express-validator/check')
const formService = require('../services/form.service')
let bcrypt = require('bcrypt-nodejs')
let multer = require('multer')
let upload = multer()
let jwt = require('jsonwebtoken')


/* POST profile edit information. */
router.post('/profile/edit/information', 
  permit(null), 
  new validator().setEditInformation(), 
  async function(req, res, next) {
  const errors = validationResult(req)
  // check if there is errors
  if (!errors.isEmpty()) {
    let paramObject = {'customArray': errors.array(), 'prop': 'param'}
    return errorMessage(res, {messageList: formService.removeDuplicates(paramObject)})
  }
  // update data to profile model
  [err, user] = await to(model.profile_information.update({
    first_name: req.body.firstName,
    last_name: req.body.firstName,
    gender: req.body.gender,
    birth_date: req.body.birthDate
  }, {where: {id: req.user.id}}))

  return successMessage(res, {message : 'the password has been changed'})
})

/* POST profile edit password. */
router.post('/profile/edit/password', 
  permit(null), 
  new validator().setEditPassword(), 
  async function(req, res, next) {
  //
  const errors = validationResult(req)
  // check if there is errors
  if (!errors.isEmpty()) {
    let paramObject = {'customArray': errors.array(), 'prop': 'param'}
    return errorMessage(res, {messageList: formService.removeDuplicates(paramObject)})
  }
  // update data to profile model
  [err, user] = await to(model.profile.update({
                        password: bcrypt.hashSync(req.body.password)
                      }, {where: {id: req.user.id}}))
  if(err) throwError('error while updating the password')
  // return success message 
  return successMessage(res, {message : 'the password has been changed'})
})

/* POST profile edit role. */
router.post('/profile/edit/role', 
  permit(null), 
  new validator().setEditRole(), 
  async function(req, res, next) {
  //
  const errors = validationResult(req)
  //
  if (!errors.isEmpty()) {
    let paramObject = {'customArray': errors.array(), 'prop': 'param'}
    return errorMessage(res, {messageList: formService.removeDuplicates(paramObject)})
  }
  // update data to profile model
  [err, user] = await to(model.profile.update({
                        role: req.body.role
                      }, {where: {id: req.user.id}}))
  if(err) throwError('error while updating the password')
  // return success message 
  return successMessage(res, {message : 'the password has been changed'})
})

/* POST profile edit contact. */
router.post('/profile/edit/contact', 
  permit(null), 
  new validator().setEditContact(), 
  async function(req, res, next) {
  //
  const errors = validationResult(req)
  //
  if (!errors.isEmpty()) {
    let paramObject = {'customArray': errors.array(), 'prop': 'param'}     
    return errorMessage(res, {messageList: formService.removeDuplicates(paramObject)})
  }
  // update data to profile model
  [err, profile_information] = await to(model.profile_information.update({
    best_way_to_reach_you: req.body.bestWayToReachYou,
    email_contact: req.body.emailContact,
    home_phone: req.body.homePhone,
    office_phone: req.body.officePhone
  }, {where: {id: req.user.profile_information_id}}))
  if(err) throwError('error while updating the password')
  // return success message 
  return successMessage(res, {message : 'the password has been changed'})
})

/* GET profile get information. */
router.post('/profile/get_information', upload.fields([]), permit(null), function(req, res, next) {

  let formData = req.body
  console.log('form data', formData)
  return res.json('ezvrf')
})

/* GET profile get information. */
router.get('/profile/get', permit(null), function(req, res, next) {

  return successMessage(res, {message : res.user})
})

module.exports = router
