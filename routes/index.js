let express = require('express')
let router = express.Router()
let models = require('../models/index')
let ssr = require('../services/ssr.service').ssr


/* GET home page. */
router.get('/', function(req, res, next) {
  const context = {
        title: 'Vue JS - Server Render',
        meta: `<meta description="vuejs server side render">`
      }
  ssr(context, req, res, next)
})

/* GET about us page. */
router.get('/about-us', function(req, res, next) {
  const context = {
        title: 'Vue JS - Server Render',
        meta: `<meta description="vuejs server side render">`
      }
  ssr(context, req, res, next)
})

/* GET vone-concept page. */
router.get('/vone-concept', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
ssr(context, req, res, next)
})

/* GET vone-advantage page. */
router.get('/vone-advantage', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
ssr(context, req, res, next)
})

/* GET vone-relationship page. */
router.get('/vone-relationship', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
ssr(context, req, res, next)
})

/* GET social-media-blog page. */
router.get('/social-media-blog', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
ssr(context, req, res, next)
})

/* GET term-of-use page. */
router.get('/term-of-use', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
ssr(context, req, res, next)
})

/* GET privacy-policy page. */
router.get('/privacy-policy', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
ssr(context, req, res, next)
})

/* GET register page. */
router.get('/register', function(req, res, next) {
  const context = {
        title: 'Vue JS - Server Render',
        meta: `<meta description="vuejs server side render">`
      }
  ssr(context, req, res, next)
})

/* GET login page. */
router.get('/login', function(req, res, next) {
  const context = {
        title: 'Vue JS - Server Render',
        meta: `<meta description="vuejs server side render">`
      }
  ssr(context, req, res, next)
})

/* GET login page. */
router.get('/notifications', function(req, res, next) {
  const context = {
        title: 'Vue JS - Server Render',
        meta: `<meta description="vuejs server side render">`
      }
  ssr(context, req, res, next)
})

/* GET login page. */
router.get('/feeds', function(req, res, next) {
  const context = {
        title: 'Vue JS - Server Render',
        meta: `<meta description="vuejs server side render">`
      }
  ssr(context, req, res, next)
})

/* GET login page. */
router.get('/activate', function(req, res, next) {
  const context = {
        title: 'Vue JS - Server Render',
        meta: `<meta description="vuejs server side render">`
      }
  ssr(context, req, res, next)
})

module.exports = router
