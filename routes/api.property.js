var express = require('express')
var router = express.Router()
var model = require('../models/index')
let validator  = require('../validator/PropertyValidator')
const { validationResult } = require('express-validator/check')
const formService = require('../services/form.service')
const permit = require('../services/auth.policy').permit
var multer  = require('multer')
var upload = multer()




router.post('/property/new', 
upload.array('propertyPhotos[]'),
// requireAuth, 
// auth.roleAuthorization('ROLE_SELLER','ROLE_LESSOR'), 
// new validator().property(), 
async function(req, res, next) {

  console.log(req.files)
  // console.log(req.body)
  //
  const errors = validationResult(req)
  // check if there is errors
  if (!errors.isEmpty()) {
    let paramObject = {'customArray': errors.array(), 'prop': 'param'}
    return errorMessage(res, {messageList: formService.removeDuplicates(paramObject)})
  }

  [err, increment] = await to(formService.getIncrement('PROPERTY'))
  if(err) {
    return errorMessage(res, err)
  }

  // update data to profile model
  [err, property] = await to(model.property.create({
    id: increment.increment_number,
    bathrooms: req.body.bathrooms,
    bedrooms: req.body.bedrooms,
    characteristics: req.body.characteristics,
    date_of_creation: Date.now(),
    description: req.body.description,
    enabled: true,
    price: req.body.price,
    sale_type: req.body.saleType,
    size: req.body.size,
    status: req.body.status,
    type: req.body.type,
    location_id: req.body.location,
    profile_id:req.user.id
  }))
  if(err) throwError('error while creating the property')

  // render data from query
  return successMessage(res, {message : 'the property has been added'})
})


router.put('/property/update', 
  permit('property'), 
  new validator().property(), 
  async function(req, res, next) {
  
  // init validation req
  const errors = validationResult(req)
  // check if there is errors
  if (!errors.isEmpty()) {
    let paramObject = {'customArray': errors.array(), 'prop': 'param'}
    return errorMessage(res, {messageList: formService.removeDuplicates(paramObject)})
  }

  // update data to profile model
  [err, property] = await to(model.property.update({
    bathrooms: req.body.bathrooms,
    bedrooms: req.body.bedrooms,
    characteristics: req.body.characteristics,
    description: req.body.description,
    enabled: true,
    price: req.body.price,
    sale_type: req.body.saleType,
    size: req.body.size,
    status: req.body.status,
    type: req.body.type,
    location_id: req.body.location
  }, {where: {id: req.body.id}}))
  if(err) throwError('error while updating the property')

  // render final message
  return successMessage(res, {message : 'the property has been updated'})
})


/* GET properties list. */
router.get('/property/:id', function(req, res, next) {
  // init limitation row according to page
  let limit = (req.query.size === undefined) ? 8 : req.query.size
  let offset = (req.query.page === undefined) ? 0 : limit * (req.query.page - 1)

  model.property.belongsTo(model.location, {foreignKey: 'location_id'})
  model.location.belongsTo(model.country, {foreignKey: 'country_id'})
  model.location.belongsTo(model.province, {foreignKey: 'province_id'})
  model.location.belongsTo(model.city, {foreignKey: 'city_id'})

  // query to load data from entity
  model.property.findOne({
    where: {id: req.param.id}
  }).then(function(entity) {
    // render data from query
    res.json({'content': entity})
  })
})


/* GET properties list. */
router.get('/properties/list', function(req, res, next) {
  // init limitation row according to page
  let limit = (req.query.size === undefined) ? 8 : req.query.size
  let offset = (req.query.page === undefined) ? 0 : limit * (req.query.page - 1)

  model.property.belongsTo(model.location, {foreignKey: 'location_id'})
  model.location.belongsTo(model.country, {foreignKey: 'country_id'})
  model.location.belongsTo(model.province, {foreignKey: 'province_id'})
  model.location.belongsTo(model.city, {foreignKey: 'city_id'})

  // query to load data from entity
  model.property.findAndCountAll({
    include: [ { model: model.location, 
      include: [
          {model: model.country}, 
          {model: model.province}, 
          {model: model.city}
        ]
    }],
    order: [
      ['id', 'DESC']
    ],
    limit: limit,
    offset: offset
  }).then(function(entity) {
    
    // render data from query
    res.json({'content': entity.rows, 'count': entity.count})
  })
})




module.exports = router