var express = require('express')
var router = express.Router()
let ssr = require('../services/ssr.service').ssr
const permit = require('../services/auth.policy').permit


/* GET home page. */
router.get('/edit/password', 
  permit(null), 
  function(req, res, next) {
  
  res.render('error', { title: 'Express' })
})

/* GET about us page. */
router.get('/edit/contact', 
  permit(null), 
  function(req, res, next) {

  res.render('error', { title: 'Express' })
})

/* GET vone-concept page. */
router.get('/edit/information', 
  permit(null), 
  function(req, res, next) {
  
  res.render('error', { title: 'Express' })
})

/* GET vone-advantage page. */
router.get('/edit/role', 
  permit(null), 
  function(req, res, next) {
  
  res.render('error', { title: 'Express' })
})

/* GET profile show page. */
router.get('/:id', function(req, res, next) {
  
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  ssr(context, req, res, next)
})

/* GET profile list page. */
router.get('/list', 
  permit(null), 
  function(req, res, next) {
  
  res.render('error', { title: 'Express' })
})

module.exports = router
