var express = require('express')
var router = express.Router()
var models = require('../models/index')
let ssr = require('../services/ssr.service').ssr



/* GET buy page. */
router.get('/', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  ssr(context, req, res, next)
})

/* GET buy page. */
router.get('/overview', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  ssr(context, req, res, next)
})




module.exports = router;
