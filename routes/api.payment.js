var express = require('express')
var router = express.Router()
var model = require('../models/index')
const filterQuery = require('../filter/FilterQueryPackage')



/* GET city page. */
router.get('/package/list', async function(req, res, next) {
  let err, package_product

  // update data to profile model
  [err, package_product] = await to(model.package_product.findAndCountAll({
    where: filterQuery.fetch(req.query),
    order: [
      ['id', 'ASC']
    ]
  }))
  if(err) {
    return errorMessage(res, err)
  }
  // render final message
  return successMessage(res, {'content': package_product.rows, 'count': package_product.count})
})




module.exports = router
