var express = require('express')
var router = express.Router()
let ssr = require('../services/ssr.service').ssr
let auth = require('../services/auth.service')



/* GET buy page. */
router.get('/', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  ssr(context, req, res, next)
})

/* GET buy page. */
router.get('/new',
// requireAuth, 
// auth.roleAuthorization(null, 'ROLE_SELLER','ROLE_LESSOR'), 
function(req, res, next) {
  console.log(req.url)
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  ssr(context, req, res, next)
})




module.exports = router;
