var express = require('express')
var router = express.Router()
var model = require('../models/index')
const Op = require('sequelize').Op
const io = require('socket.io')()



/* GET city page. */
router.get('/city', async function(req, res, next) {
  let err, city
  model.city.belongsTo(model.province, {foreignKey: 'province_id'})
  model.province.belongsTo(model.country, {foreignKey: 'country_id'})
  // query to load data from entity
  model.city.findAndCountAll({
    where: {
      [Op.or]: [{name: { $iLike: '%' + req.query.q + '%' }}, {name_ascii: { $iLike: '%' + req.query.q + '%' }}]
    },
    include: [ { model: model.province,
      include: [
        {model: model.country}
      ]
    } ],
    order: [
      ['id', 'DESC']
    ],
    limit: 50
  }).then(function(entity) {

    successMessage(res, {'content': entity.rows, 'count': entity.count})
  })
})




module.exports = router
