var express = require('express')
var router = express.Router()
const fs = require('fs')
var models = require('../models/index')
let ssr = require('../services/ssr.service').ssr



/* GET buy page. */
router.get('/', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  ssr(context, req, res, next)
})

/* GET buy page. */
router.get('/:id(\\d+)', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  ssr(context, req, res, next)
})

/* GET buy page. */
router.get('/:id(\\d+)/proposition', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  ssr(context, req, res, next)
})

/* GET buy page. */
router.get('/:id(\\d+)/statistics', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  ssr(context, req, res, next)
})

/* GET buy page. */
router.get('/:id(\\d+)/settings', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  ssr(context, req, res, next)
})




/* GET buy page. */
router.get('/list', function(req, res, next) {
  const context = {
    title: 'Vue JS - Servfzefer Render',
    meta: `<meta description="vuejs server side render">`
  }
  ssr(context, req, res, next)
})

module.exports = router
