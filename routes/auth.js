let express = require('express')
let router = express.Router()
let bcrypt = require('bcrypt-nodejs')
let validator  = require('../validator/AuthValidator')
let model = require('../models/index')
const { validationResult } = require('express-validator/check')
const formService = require('../services/form.service')
const email = require('../services/email.service')
const auth = require('../services/auth.service')

/* POST register method. */
router.post('/register', new validator().setRegister(), function(req, res, next) {
  console.log(req.body)
  // get the ip address
  var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress

  // const for validation result
  const errors = validationResult(req)
  
  // show errors if the form doesn't meet the validation
  if (!errors.isEmpty()) {
    let paramObject = {'customArray': errors.array(), 'prop': 'param'}
    return errorMessage(res, {messageList: formService.removeDuplicates(paramObject)})
  } 

  // query to load increment for profile information data
  formService.getAutoIncrement('PROFILE_INFORMATION').then(function(result){
    let newId = ++result.dataValues.increment_number
    formService.saveAutoIncrement({id: 'PROFILE_INFORMATION', number: newId})

    // save data to profile information
    model.profile_information.create({
      id: newId,
      first_name: req.body.firstName,
      last_name: req.body.lastName,
      birth_date: req.body.birthDate
    }).then(profileInformation => {

      // query to load increment for profile data
      formService.getAutoIncrement('PROFILE').then(function(result){
      let profileId = ++result.dataValues.increment_number
      formService.saveAutoIncrement({id: 'PROFILE', number: profileId})

      // save data to profile information
      model.profile.create({
        id: profileId,
        account_non_expired: true,
        account_non_locked: true,
        credential: req.body.credential,
        credentials_non_expired: true,
        date_of_creation: Date.now(),
        date_of_creation_token: Date.now(),
        enabled: false,
        ip_address: ip,
        password: bcrypt.hashSync(req.body.password),
        role: 'ROLE_LIMITED',
        token: formService.generateToken(),
        profile_information_id: profileInformation.dataValues.id
      }).then(profile => {
        email.sendMail({login: profile.credential, lastName: profileInformation.last_name, token: profile.token}, 'user registration')
        return successMessage(res, {message : `an email has been sent to ${profile.credential}. click on the link to activate your account`})
    })
  })
    })
  })
})

/* POST recover method. */
router.post('/recover', new validator().setRecover(), function(req, res, next) {
  
  // const for validation result
  const errors = validationResult(req)

  // show errors if the form doesn't meet the validation
  if (!errors.isEmpty()) {
    let paramObject = {'customArray': errors.array(), 'prop': 'param'}
    return errorMessage(res, {messageList: formService.removeDuplicates(paramObject)})
  }

  // update profile table give a token to update password
  model.profile.update(
    {token: formService.generateToken()},
    {where: {credential: req.body.credential}}
  ).then(profile => {
     
  })

  // query to load data from entity
  model.profile.findOne({
    where: {credential: req.body.credential}
  }).then(function(profile) {
    
    // send email to user
    email.sendMail(profile.credential, 'user recover', profile)

    // render data from query
    res.status(200).json({field: 'label.success', message: 'Parcel Pending API'})
  })
})

/* GET login page. */
router.post('/login', async function(req, res, next) {
  [err, token] = await to(auth.login(req.body))
  if(err) {
    return errorMessage(res, err)
  }
  return successMessage(res, {message : token})
})

/* GET login page. */
router.get('/activate', async function(req, res, next) {
  if(!req.query.t) {
    return errorMessage(res, {message : 'something wrong with the token'})
  }

  [err, profile] = await to(model.profile.findOne({ where: { token: req.query.t } }))

  if(err){
    return errorMessage(res, {message : err})
  }

  if (!profile) {
    return errorMessage(res, {message : 'the token has expired'})
  }

  // update profile table give a token to update password
  [err, profileUpdate] = await to(model.profile.update({
    token: null,
    enabled: true },
    {where: {id: profile.id}}
  ))

  const userObject = { id: profile.id, login: profile.credential, time: new Date() } 
  token = auth.issueToken(userObject) 

  return successMessage(res, {message : token})
})

module.exports = router
