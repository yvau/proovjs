var express = require('express')
var router = express.Router()
const { validationResult } = require('express-validator/check')
let permit = require('../services/auth.policy').permit
const formService = require('../services/form.service')
let model = require('../models/index')
let validator  = require('../validator/SearchProposalValidator')

router.post('/lease/new', 
  permit(null, 'ROLE_LESSOR'),
  new validator().leaseForm(),
  async function(req, res, next) {
    //
    const errors = validationResult(req)
    // check if there is errors
    if (!errors.isEmpty()) {
      let paramObject = {'customArray': errors.array(), 'prop': 'param'}
      return errorMessage(res, {messageList: formService.removeDuplicates(paramObject)})
    }

    [err, increment] = await to(formService.getIncrement('SEARCH_PROPOSAL'))
    if(err) {
      return errorMessage(res, err)
    }

    // update data to profile model
  [err, search_proposal] = await to(model.search_proposal.create({
    id: increment.increment_number,
    age_of_property: req.body.ageOfProperty,
    amenity: req.body.amenity,
    category_of_property: req.body.categoryOfProperty,
    has_garage: req.body.hasGarage,
    has_lift: req.body.hasLift,
    has_parking: req.body.hasParking,
    has_pool: req.body.hasPool,
    is_first_buyer: req.body.isFirstBuyer,
    is_furnished: req.body.isFurnished,
    is_near_navigable_body_of_water: req.body.isNearNavigableBodyOfWater,
    is_near_parc: req.body.isNearParc,
    is_near_public_transport: req.body.isNearPublicTransport,
    is_near_trade_square : req.body.isNearTradeSquare,
    is_pre_approved: req.body.isPreApproved,
    is_pre_qualified: req.body.isPreQualified,
    is_suitable_for_reduced_mobility: req.body.isSuitableForReducedMobility,
    is_urgent: req.body.isUrgent,
    is_without_contingency: req.body.isWithoutContingency,
    price_maximum: req.body.priceMaximum,
    price_minimum: req.body.priceMinimum,
    type_of_property: req.body.typeOfProperty,
    type_of_proposal: req.body.typeOfProposal,
    type_of_search: req.body.typeOfSearch,
    profile_id: req.user.id
  }))
  if(err) throwError('error while creating the property')

  // render data from query
  return successMessage(res, {message : 'the selling profile has been added'})
})


router.put('/lease/update', 
  permit('search_proposal'),
  new validator().sellForm(),
  async function(req, res, next) {
    //
    const errors = validationResult(req)
    // check if there is errors
    if (!errors.isEmpty()) {
      let paramObject = {'customArray': errors.array(), 'prop': 'param'}
      return errorMessage(res, {messageList: formService.removeDuplicates(paramObject)})
    }

    // update data to profile model
  [err, search_proposal] = await to(model.search_proposal.create({
    age_of_property: req.body.ageOfProperty,
    amenity: req.body.amenity,
    category_of_property: req.body.categoryOfProperty,
    has_garage: req.body.hasGarage,
    has_lift: req.body.hasLift,
    has_parking: req.body.hasParking,
    has_pool: req.body.hasPool,
    is_first_buyer: req.body.isFirstBuyer,
    is_furnished: req.body.isFurnished,
    is_near_navigable_body_of_water: req.body.isNearNavigableBodyOfWater,
    is_near_parc: req.body.isNearParc,
    is_near_public_transport: req.body.isNearPublicTransport,
    is_near_trade_square : req.body.isNearTradeSquare,
    is_pre_approved: req.body.isPreApproved,
    is_pre_qualified: req.body.isPreQualified,
    is_suitable_for_reduced_mobility: req.body.isSuitableForReducedMobility,
    is_urgent: req.body.isUrgent,
    is_without_contingency: req.body.isWithoutContingency,
    price_maximum: req.body.priceMaximum,
    price_minimum: req.body.priceMinimum,
    type_of_property: req.body.typeOfProperty,
    type_of_proposal: req.body.typeOfProposal,
    type_of_search: req.body.typeOfSearch,
    profile_id: req.user.id,

  }, {where: {id: req.body.id}}))
  if(err) throwError('error while updating the leasing profile')

  // render data from query
  return successMessage(res, {message : 'the leasing profile has been updated'})
})
module.exports = router