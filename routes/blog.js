var express = require('express')
var router = express.Router()
const fs = require('fs')
var models = require('../models/index')
let ssr = require('../services/ssr.service').ssr


/* GET home page. */
router.get('/rezfez', function(req, res, next) {
  models.country.hasMany(models.province, {foreignKey: 'country_id', constraints: false})
  models.country.find({
    where: { id: 'CA' },
	include: [ { model: models.province } ]
  }).then(function(country) {
    res.json(country)
  })
})

/* GET home page. */
router.get('/', function(req, res, next) {
  const context = {
        title: 'Vue JS - Server Render',
        meta: `<meta description="vuejs server side render">`
      }
  ssr(context, req, res, next)
})

/* GET about us page. */
router.get('/about-us', function(req, res, next) {
  const context = {
        title: 'Vue JS - Server Render',
        meta: `<meta description="vuejs server side render">`
      }
  ssr(context, req, res, next)
})

/* GET vone-concept page. */
router.get('/vone-concept', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
ssr(context, req, res, next)
})

/* GET vone-advantage page. */
router.get('/vone-advantage', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
ssr(context, req, res, next)
})

/* GET vone-relationship page. */
router.get('/vone-relationship', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
ssr(context, req, res, next)
})

/* GET social-media-blog page. */
router.get('/social-media-blog', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
ssr(context, req, res, next)
})

/* GET term-of-use page. */
router.get('/term-of-use', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
ssr(context, req, res, next)
})

/* GET privacy-policy page. */
router.get('/privacy-policy', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
ssr(context, req, res, next)
})

/* GET show blog id. */
router.get('/:id', function(req, res, next) {
  const context = {
        title: 'Vue JS - Server Render',
        meta: `<meta description="vuejs server side render">`
      }
  ssr(context, req, res, next)
})

module.exports = router
