let express = require('express')
let router = express.Router()
let model = require('../models/index')
let validator  = require('../validator/ProposalValidator')
const { validationResult } = require('express-validator/check')
const formService = require('../services/form.service')
let permit = require('../services/auth.policy').permit

/* POST buy new. */
router.post('/buy/new', 
  permit(null, 'ROLE_BUYER'), 
  new validator().buyNew(), 
  async function(req, res, next) {
    //

    const errors = validationResult(req)
    // check if there is errors
    if (!errors.isEmpty()) {
      let paramObject = {'customArray': errors.array(), 'prop': 'param'}
      return errorMessage(res, {messageList: formService.removeDuplicates(paramObject)})
    }
  
    [err, increment] = await to(formService.getIncrement('PROPOSAL'))
    if(err) {
      return errorMessage(res, err)
    }
  
    // update data to profile model
    [err, buy] = await to(model.proposal.create({
      id: increment.increment_number,
      age_of_property: req.body.ageOfProperty,
      bathrooms: req.body.bathrooms,
      bedrooms: req.body.bedrooms,
      enabled: true,
      features: req.body.features,
      is_urgent: req.body.urgent,
      price_maximum: req.body.priceMaximum,
      price_minimum: req.body.priceMinimum,
      size: req.body.size,
      status: req.body.status,
      date_of_creation: Date.now(),
      type_of_property: req.body.typeOfProperty,
      type_of_proposal: req.body.typeOfProposal,
      profile_id: req.user.id
    }))
    if(err) throwError('error while updating the password')
  
    return successMessage(res, {message : 'the password has been changed'})
})


/* POST buy new. */
router.put('/buy/update', 
permit('proposal'),
  new validator().buyNew(), 
  async function(req, res, next) {
    //

    const errors = validationResult(req)
    // check if there is errors
    if (!errors.isEmpty()) {
      let paramObject = {'customArray': errors.array(), 'prop': 'param'}
      return errorMessage(res, {messageList: formService.removeDuplicates(paramObject)})
    }
  
    [err, increment] = await to(formService.getIncrement('PROPOSAL'))
    if(err) {
      return errorMessage(res, err)
    }
  
    // update data to profile model
    [err, buy] = await to(model.proposal.create({
      age_of_property: req.body.ageOfProperty,
      bathrooms: req.body.bathrooms,
      bedrooms: req.body.bedrooms,
      enabled: true,
      features: req.body.features,
      is_urgent: req.body.urgent,
      price_maximum: req.body.priceMaximum,
      price_minimum: req.body.priceMinimum,
      size: req.body.size,
      status: req.body.status,
      type_of_property: req.body.typeOfProperty,
      type_of_proposal: req.body.typeOfProposal,
      profile_id: req.user.id
    }, {where: {id: req.body.id}}))
    if(err) throwError('error while updating the password')
  
    // render final message
    return successMessage(res, {message : 'the password has been changed'})
})

  module.exports = router
